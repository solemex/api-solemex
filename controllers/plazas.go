package controllers

import (
	"gitlab.com/solemex/api-solemex/model"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)

func ListPlaza(c *gin.Context) {
	plaza, err := model.ListPlaza() //plaza listplaza
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, plaza)
}

func GetPlaza(c *gin.Context) { //OBTENER plaza
	id := c.Param("id")
	plaza, err := model.GetPlaza(id) //plaza getplaza
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, plaza)
}

func InsertPlaza(c *gin.Context) { //INSERTA A CLIENTE
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var plaza model.Plaza
	err = json.Unmarshal(body, &plaza)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertPlaza(plaza)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}

func UpdatePlaza(c *gin.Context) {
	id := c.Param("id")                         //USA  PUT
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var plaza model.Plaza
	err = json.Unmarshal(body, &plaza)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	plaza.ID = id
	err = model.UpdatePlaza(plaza)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}

func DeletePlaza(c *gin.Context) { //eliminar CLIENTE
	id := c.Param("id")
	err := model.DeletePlaza(id) //cliente getcliente
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Eliminado correctamente")
}
