package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

func ConPendTodas(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var contarjetas model.ConTarjetas
	err = json.Unmarshal(body, &contarjetas)
	fmt.Println(contarjetas)

// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		return
// 	}

	datos, err := model.ConPendTodas(contarjetas.Idcliente, contarjetas.Idservicio)
	if err != nil {
		c.JSON(200, "Falla en Consulta.")
		return
	}

	c.JSON(200, datos)
}

func ConPendAsig(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var contarjetas model.ConTarjetas
	err = json.Unmarshal(body, &contarjetas)
	fmt.Println(contarjetas)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.ConPendAsig(contarjetas.Idcliente, contarjetas.Idservicio)
	if err != nil {
		c.JSON(200, "Falla en Consulta.")
		return
	}

	c.JSON(200, datos)

}

func ConPendNoAsig(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var contarjetas model.ConTarjetas
	err = json.Unmarshal(body, &contarjetas)
	fmt.Println(contarjetas)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.ConPendNoAsig(contarjetas.Idcliente, contarjetas.Idservicio)
	if err != nil {
		c.JSON(200, "Falla en Consulta.")
		return
	}

	c.JSON(200, datos)

}

func ConEntregadas(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var contarjetas model.ConTarjetas
	err = json.Unmarshal(body, &contarjetas)
	fmt.Println(contarjetas)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.ConEntregadas(contarjetas.Idcliente, contarjetas.Idservicio)
	if err != nil {
		c.JSON(200, "Falla en Consulta.")
		return
	}

	c.JSON(200, datos)

}

func ConDevueltas(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var contarjetas model.ConTarjetas
	err = json.Unmarshal(body, &contarjetas)
	fmt.Println(contarjetas)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.ConDevueltas(contarjetas.Idcliente, contarjetas.Idservicio)
	if err != nil {
		c.JSON(200, "Falla en Consulta.")
		return
	}

	c.JSON(200, datos)

}
