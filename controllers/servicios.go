package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

func ListServicios(c *gin.Context) {
	servicio, err := model.ListServicios()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, servicio)
}

func GetServicios(c *gin.Context) {
	id := c.Param("id")
	servicio, err := model.GetServicios(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, servicio)
}

func InsertServicios(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var servicio model.Servicio
	err = json.Unmarshal(body, &servicio)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertServicios(servicio)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}

func UpdateServicios(c *gin.Context) {
	id := c.Param("id")
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var servicio model.Servicio
	err = json.Unmarshal(body, &servicio)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	servicio.Id = id
	err = model.UpdateServicios(servicio)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}

func DeleteServicios(c *gin.Context) {
	id := c.Param("id")
	err := model.DeleteServicios(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Eliminado correctamente")
}
