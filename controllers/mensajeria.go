package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
	"fmt"
)

// ListMenxMia
func ListMenxMia(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var idmia model.Idmia
	err = json.Unmarshal(body, &idmia)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("ID mensajeria ",idmia.Idmensajeria)
	id := idmia.Idmensajeria
	datos, err := model.ListMenxMia(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}





func ListMensajeria(c *gin.Context) {
	mensajeria, err := model.ListMensajeria()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, mensajeria)
}

func GetMensajeria(c *gin.Context) {
	id := c.Param("id")
	mensajeria, err := model.GetMensajeria(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, mensajeria)
}

func InsertMensajeria(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var mensajeria model.Mensajeria
	err = json.Unmarshal(body, &mensajeria)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertMensajeria(mensajeria)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}

func UpdateMensajeria(c *gin.Context) {
	id := c.Param("id")
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var mensajeria model.Mensajeria
	err = json.Unmarshal(body, &mensajeria)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	mensajeria.Id = id
	err = model.UpdateMensajeria(mensajeria)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}

func DeleteMensajeria(c *gin.Context) {
	id := c.Param("id")
	err := model.DeleteMensajeria(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Eliminado correctamente")
}
