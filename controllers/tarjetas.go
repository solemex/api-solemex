package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
	// "strconv"
)



func GetSuperTarj(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var super model.Supervision
	err = json.Unmarshal(body, &super)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	datos, err := model.GetSuperTarj(super.Mensajero, super.Fecha_entrega)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}


func ListCatTarjetas(c *gin.Context) {
	tarjetas, err := model.ListCatTarjetas() //tarjetas listtarjetas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, tarjetas)
}

func GetCatTarjetas(c *gin.Context) { //OBTENER tarjetas
	id := c.Param("id")
	tarjetas, err := model.GetCatTarjetas(id) //tarjetas gettarjetas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, tarjetas)
}

func GetTarxRem(c *gin.Context) { //OBTENER tarjetas
	idremesa := c.Param("idremesa")
	tarjetas, err := model.GetTarxRem(idremesa) //tarjetas gettarjetas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, tarjetas)
}

func BuscarFolio(c *gin.Context) { //OBTENER tarjetas

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var folio model.Folio
	err = json.Unmarshal(body, &folio)
	fmt.Println(folio)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.BuscarFolio(folio.Numguia)
	if err != nil {
		c.JSON(200, "Falla en Consulta.")
		return
	}

	c.JSON(200, datos)
}

func TarjetasxEstatus(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var tarjetas model.TarjxEstatus
	// Validar entrada
	err = json.Unmarshal(body, &tarjetas)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	datos, err := model.TarjxStatus(tarjetas.Estatus, tarjetas.Idcliente)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)

}
