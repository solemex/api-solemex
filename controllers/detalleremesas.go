package controllers


import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

//Reporte de Salida
func DetalleRemesas(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var detalle model.Numrem
	// Validar entrada
	err = json.Unmarshal(body, &detalle)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	datos, err := model.DetalleRemesas(detalle.Numrem)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)

}
