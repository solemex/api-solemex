//Controlador Mensajeros

package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)


func ListMensajeros(c *gin.Context) {
	mensajeros, err := model.ListMensajeros() //mensajeros listmensajeros
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, mensajeros)
}

func GetMensajeros(c *gin.Context) { //OBTENER CLIENTE
	id := c.Param("id")
	mensajeros, err := model.GetMensajeros(id) //cliente getcliente
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, mensajeros)
}

func InsertMensajeros(c *gin.Context) { //INSERTA A CLIENTE
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var mensajeros model.Mensajeros
	err = json.Unmarshal(body, &mensajeros)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertMensajeros(mensajeros)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}

func UpdateMensajeros(c *gin.Context) {
	id := c.Param("id")                         //USA  PUT
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var mensajeros model.Mensajeros
	err = json.Unmarshal(body, &mensajeros)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	mensajeros.ID = id
	err = model.UpdateMensajeros(mensajeros)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}

func DeleteMensajeros(c *gin.Context) {
	id := c.Param("id")
	err := model.DeleteMensajeros(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Eliminado correctamente")
}

//INSERTAR A LA TABLA
func ValidaInfo(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		fmt.Println(" Err= ", err)
		c.JSON(500, err.Error())
		return
	}

	var mensajeros []model.Mensajeros
	err = json.Unmarshal(body, &mensajeros)
	if err != nil {
		fmt.Println("Err =", err)
		c.JSON(500, err.Error())
		return
	}

	// Preparando Datos
	var total = len(mensajeros)
	fmt.Println("Total", total)

	// CICLO
	for i := 0; i < total; i++ {
		//Mandar llamar BuscarIdKey
		var Nomuser = mensajeros[i].Nomuser

		//fmt.Println("Nomuser)

		// Si no existe, Insertar. Si existe, error.
		idkey, err := model.BusIdValuser(Nomuser)
		fmt.Println("Nomuser", mensajeros[i].Nomuser)

		if idkey.ID == "" {
			fmt.Println("if", mensajeros)
			err2 := model.InsertMensajeros(mensajeros[i])
			if err2 != nil {
				fmt.Println("Err =", err2)
				c.JSON(500, err2.Error())
				return
			} else {
				c.JSON(200, "Nombre de usuario disponible para su uso")
			}

		} else {

			fmt.Println("Nombre de usuario ya esta en uso", mensajeros)
			c.JSON(500, "Nombre de usuario ya existente")

		}

		if err != nil {
			fmt.Println("idkey ", idkey.ID)

		} else {
			fmt.Println("error", err)
		}

	}
}

func ListMensajerosxMia(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var idmia model.Mensajeros
	err = json.Unmarshal(body, &idmia)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("ID mensajeria ",idmia.Idmensajeria)
	id := idmia.Idmensajeria
	datos, err := model.ListMensajerosxMia(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}


// func ValidaInfo(c *gin.Context) {
// 	body, err := ioutil.ReadAll(c.Request.Body)

// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		fmt.Println(err)
// 		return
// 	}

// 	var mensajeros []model.mensajeros
// 	err = json.Unmarshal(body, &valuser)
// 	if err != nil {
// 		fmt.Println(err)
// 		c.JSON(500, err.Error())
// 		return
// 	}

// 	datos, err := model.ValidarInfo(Nomuser)
// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		fmt.Println(err)
// 		return
// 	}
// 	if valuser.Nomuser == valuser.Nomuser {
// 		fmt.Println("Este usuario ya existe")
// 		c.JSON(500, err.Error())
// 		return
// 	}
// 	if valuser.Nomuser == "" {
// 		fmt.Println("Favor de escribir su nombre de usuario")
// 		c.JSON(500, err.Error())
// 		return
// 	}
// 	// if valuser.Nomuser != valuser.Nomuser
// 	c.JSON(200, datos)
// }
