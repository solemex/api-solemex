package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

// Consultar avisos del mensajero 
func AvisosxMens (c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var avisosxm model.Avisosxm
	// Validar entrada
	err = json.Unmarshal(body, &avisosxm)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	datos, err := model.GetAvisosxM(avisosxm.Idmensajero)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}

func ListAvisos(c *gin.Context) {
	avisos, err := model.ListAvisos()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, avisos)

}

//Insertar aviso.
func InsertAvisos(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var avisos model.Avisos
	err = json.Unmarshal(body, &avisos)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertAvisos(avisos)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Insertado correctamente")
}

// AddPrioridad PUT  Manda Barcode Numguia o Numcli

//Insertar aviso.
func AddPrioridad(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var prioridad model.Prioridad
	err = json.Unmarshal(body, &prioridad)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	err = model.AddPrioridad(prioridad)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Actualizada prioridad correctamente")
}





//CONSULTA CON PARAMETROS
func InsertAviso(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		fmt.Println("Err =", err)
		c.JSON(500, err.Error())
		return
	}

	var avisos []model.Avisos
	err = json.Unmarshal(body, &avisos)
	if err != nil {
		fmt.Println("Err =", err)
		c.JSON(500, err.Error())
		return
	}

	// Preparando Datos
	var total = len(avisos)
	fmt.Println("Total", total)

	// CICLO
	for i := 0; i < total; i++ {
		//Mandar llamar BuscarIdKey
		var Idguia = avisos[i].Idguia
		var Idmensajero = avisos[i].Idmensajero

		//fmt.Println("Idguia  Idmensajero ", Idguia, Idmensajero )

		// Si Existe Update, si Err POST
		idkey, err := model.BusIdAvisos(Idguia, Idmensajero)
		fmt.Println("Idguia, Idmensajero", avisos[i].Idguia, avisos[i].Idmensajero)

		if idkey.Id == "" {
			fmt.Println("if", avisos)
			err2 := model.InsertAviso(avisos[i])
			if err2 != nil {
				fmt.Println("Err =", err2)
				c.JSON(500, err2.Error())
				return
			} else {
				c.JSON(200, "Insertado correctamente")
			}

		} else {

			fmt.Println("Else", avisos)
			err3 := model.UpdateAvisos(avisos[i])
			if err3 != nil {
				fmt.Println("Err =", err3)
				c.JSON(500, err3.Error())
				return
			} else {
				c.JSON(200, "Actualizado correctamente")
			}

		}

		if err != nil {
			fmt.Println("idkey ", idkey.Id)

		} else {
			fmt.Println("error", err)
		}

	}
}
