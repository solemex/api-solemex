package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

//LOGIN APP Post Con Parametros
func LoginApp(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var loginpar model.LoginPar
	err = json.Unmarshal(body, &loginpar)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	//fmt.Println("Nomuser", loginpar.Nomuser)
	fmt.Println("Password", loginpar.Password)

	datos, err := model.LoginAppMovil(loginpar.Nomuser, loginpar.Password)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
	//	fmt.Println("datos ", datos)
}
