package controllers

import (
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"github.com/gin-gonic/gin"
)

func SubirImagenFirma(c *gin.Context) {

	file, _ := c.FormFile("file")
	log.Println(file.Filename)
	// Upload the file to specific dst.
		filename := filepath.Base(file.Filename)
		uploadPath := "./imagenes/firmas/" + filename

		if err := c.SaveUploadedFile(file, uploadPath); err != nil {
		  c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		  return
		}

	c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
}

func SubirImagenAcuses(c *gin.Context) {

	file, _ := c.FormFile("file")
	log.Println(file.Filename)
	// Upload the file to specific dst.
		filename := filepath.Base(file.Filename)
		uploadPath := "./imagenes/acuses/" + filename

		if err := c.SaveUploadedFile(file, uploadPath); err != nil {
		  c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		  return
		}

	c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
}

func SubirImagenDomicilio(c *gin.Context) {

	file, _ := c.FormFile("file")
	log.Println(file.Filename)
	// Upload the file to specific dst.
		filename := filepath.Base(file.Filename)
		uploadPath := "./imagenes/domicilios/" + filename

		if err := c.SaveUploadedFile(file, uploadPath); err != nil {
		  c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
		  return
		}

	c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
}
