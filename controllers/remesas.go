package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
	"strconv"
)

func ListRemesas(c *gin.Context) {
	remesas, err := model.ListRemesas() //remesas listremesas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, remesas)
}


//Controlador para Consulta de Remesas x Status
func EstatusRem(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var statrem model.StatRem
	// Validar entrada
	err = json.Unmarshal(body, &statrem)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// mandar consultar
	datos, err := model.EstatusRem(statrem.Status, statrem.Idcliente)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)

}

func RemxValidar (c *gin.Context) {

	datos, err := model.RemxValidar() 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}



func GetRemesas(c *gin.Context) { //OBTENER remesas
	id := c.Param("id")
	remesas, err := model.GetRemesas(id) //remesas getremesas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, remesas)
}

// POST REMESAS Crear Remesa
func InsertRemesas(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var remesas model.Remesas
	err = json.Unmarshal(body, &remesas)

	fmt.Println("numrem ", remesas.Numrem)
	//fmt.Println("Tarjetas " , remesas.Tarjetas)

	if remesas.Tot == 0 {
		c.JSON(400, "No hay tarjatas en el archivo.")
		return
	}

	if remesas.Numrem == "" {
		c.JSON(400, "Remesa No Existe.")
		return
	}

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// Ejecutar modelo de Grabar Datos.
	err = model.InsertRemesas(remesas)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// LLAMAR IDget
	idRem, err2 := model.GetidRem()
	if err2 != nil {
		c.JSON(500, err2.Error())
		return
	}

	// Llamar a Tarjetas
	fmt.Println("Remesa ID ", idRem[0].ID)

	
	// Ciclo dentro de Remesas. Dentro de Tarjetas.
	for i := 0; i < remesas.Tot; i++ {

		// LLAMAR IDget
		var tarjetas model.Tarjetas
		tarjetas = remesas.Tarjetas[i]

		// s := strconv.Itoa(-42)
		idfolio := strconv.Itoa(i + 1)

		err := model.InsertTarjetas(tarjetas, idRem, idfolio)
		if err != nil {
			c.JSON(500, err.Error())
			return
		}
	}
	c.JSON(200, idRem)

}


//Controlador para Consulta de Remesas x Status
func ProcesaRem(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var terminar model.Terminar
	// Validar entrada
	err = json.Unmarshal(body, &terminar)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	fmt.Println(terminar)
	err = model.ProcesaRem(terminar)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Remesa Procesada")

}

//Controlador para Consulta de Remesas x Status
func CerrarRem(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var cerrar model.Cerrar
	// Validar entrada
	err = json.Unmarshal(body, &cerrar)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	fmt.Println(cerrar)
	err = model.CerrarRem(cerrar)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Remesa Procesada")

}


//Controlador para Consulta de Remesas x Status
func EstatusRemAlta(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var statrem model.StatRem
	// Validar entrada
	err = json.Unmarshal(body, &statrem)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// mandar consultar
	datos, err := model.EstatusRemAlta(statrem.Status)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)

}



