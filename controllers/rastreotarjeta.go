package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)


func RastreoIntentos(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var rastreoInt model.Intentos
	err = json.Unmarshal(body, &rastreoInt)
	fmt.Println(rastreoInt)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.RastreoIntentos(rastreoInt.Numguia)
	if err != nil {
		c.JSON(200, err.Error() )
		return
	}

	c.JSON(200, datos)

}

func RastreoTarjxSuc(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var rastreoxsuc model.Rastreo
	err = json.Unmarshal(body, &rastreoxsuc)
	fmt.Println(rastreoxsuc)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.RastreoTarjxSuc(rastreoxsuc.Numguia)
	if err != nil {
		c.JSON(200, err.Error() )
		return
	}

	c.JSON(200, datos)

}

func RastreoTarjeta(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.ConTarjetas() //cattarjetas conpendtodas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var rastreo model.Rastreo
	err = json.Unmarshal(body, &rastreo)
	fmt.Println(rastreo)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.RastreoTarjeta(rastreo.Numguia)
	if err != nil {
		c.JSON(200, err.Error() )
		return
	}

	c.JSON(200, datos)

}
