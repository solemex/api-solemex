package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
	// "fmt"
)

func TarjSinSuc(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var bustarj model.BusTarj
	err = json.Unmarshal(body, &bustarj)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.TarjSinSuc(bustarj.Numrem, bustarj.Idsucbr)
	if err != nil {
		c.JSON(500, err.Error())
		return
	} else {
		c.JSON(200, datos)
	}
	
}

func PutIdSucBr(c *gin.Context) {
	id := c.Param("id")                         //USA  PUT
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var pusidsuc model.PutIdSuc
	err = json.Unmarshal(body, &pusidsuc)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	pusidsuc.Id = id
	err = model.PutIdSucBr(pusidsuc)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}
