package controllers

import (
//	"encoding/json"
//	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
//	"io/ioutil"
)


// Obtener el Email de un ID en Tarjetas
func DevolucionDirecta(c *gin.Context) { 
	numguia := c.Param("numguia")
	
	datos , err := model.DevolucionDirecta(numguia) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}
