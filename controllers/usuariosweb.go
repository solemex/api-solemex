package controllers

import (
	"fmt"
	"gitlab.com/solemex/api-solemex/model"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)


func ListUsuariosxCli(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var idcli model.GetUsers
	err = json.Unmarshal(body, &idcli)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("ID CLIENTE ",idcli.Idcliente)
	id := idcli.Idcliente
	datos, err := model.ListUsuariosxCli(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}


func ListUsuariosxMia(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var idmia model.GetUsers
	err = json.Unmarshal(body, &idmia)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("ID mensajeria ",idmia.Idmensajeria)
	id := idmia.Idmensajeria
	datos, err := model.ListUsuariosxMia(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}


// POST Función para Activar Usuarios   ActivarUsuario
func ActivarUsuario(c *gin.Context) { 
	id := c.Param("id")

	fmt.Println("IDusuariosweb", id)

	var activar model.Activar
	activar.Idusuariosweb = id

	err := model.ActivarUsuario(activar)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Usuario Activado correctamente")
}


// Funcion para obtener el ID de un email
func GetidxMail(c *gin.Context) { 
	body, err := ioutil.ReadAll(c.Request.Body) 

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var getid  model.Getid
	err = json.Unmarshal(body, &getid)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	

	datos, err2 := model.GetidxMail(getid.Email)
	
	if err != nil {
		c.JSON(500, err2.Error())
		return
	}
	c.JSON(200, datos)
}




func ListUsuariosWeb(c *gin.Context) {
	fmt.Println("ListUsuariosWeb")
	usuariosweb, err := model.ListCatUsr() 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, usuariosweb)
}

func GetUsuariosWeb(c *gin.Context) { 
	id := c.Param("id")

	usuariosweb, err := model.GetUsuariosWeb(id) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, usuariosweb)
}


func InsertUsuariosWeb(c *gin.Context) { 
	body, err := ioutil.ReadAll(c.Request.Body) 

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var usuariosweb model.InsertUsuariosW
	err = json.Unmarshal(body, &usuariosweb)
	
	err = model.InsertUsuariosWeb(usuariosweb)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}



//Para validar el login
func BuscarUsuario(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	//var usuariosweb model.UsuariosWeb
	var userweb model.Usuario

	err = json.Unmarshal(body, &userweb)
	fmt.Println("email que recibo", userweb.Email, userweb.Password)
	
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	
	datos, err := model.BuscarUsuario(userweb.Email, userweb.Password)
	if err != nil {
		c.JSON(200, err.Error())
		return
	}
	c.JSON(200, datos)
	fmt.Println("datos ", datos)
}


func DeleteUsuariosWeb(c *gin.Context) { 
	id := c.Param("id")
	err := model.DeleteUsuariosWeb(id) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Eliminado correctamente")
}

func UpdateUsuariosWeb(c *gin.Context) {
	id := c.Param("id")                         
	body, err := ioutil.ReadAll(c.Request.Body) 

	//fmt.Println("body",body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var putusuariosweb model.PutUsuariosWeb
	fmt.Println("usuariosweb", putusuariosweb)

	err = json.Unmarshal(body, &putusuariosweb)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	putusuariosweb.IDusuariosweb = id
	
	err = model.UpdateUsuariosWeb(putusuariosweb)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
	//fmt.Println("respuesta", body)

}
