package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

//Post para Grabar Rem x Estado Mandando idremesa
func GetCantxEstado(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var remxest model.RemxEst
	err = json.Unmarshal(body, &remxest)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("idremesa", remxest.Idremesa )

	datos, err := model.GetCantxEstado(remxest.Idremesa)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, datos)

	// RECORRER datos para agregarlos a la tabla de Remxplaza.	
	fmt.Println("Remesa datos ID ", datos[0].Idremesa)
	

	var totalrxe int = len (datos)
	fmt.Println("total datos", totalrxe)


	// Ciclo dentro de Remesas. Dentro de Tarjetas.
	for i := 0; i < totalrxe ; i++ {

		fmt.Println(datos[i] )

		 // var remxest model.RemxEst

		 remxest = datos[i]

		err := model.AddRemxPlaza(remxest)
		if err != nil {
			c.JSON(500, err.Error())
			return
		}
	}
	c.JSON(200, "rem x est agregadas")

}







//Trae todos
func RemxPlaza(c *gin.Context) {
	remxplaza, err := model.RemxPlaza()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, remxplaza)
}



func UpdateRemxPlaza(c *gin.Context) {
	//Obtener 
	body, err := ioutil.ReadAll(c.Request.Body) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	//Validar Reespuedta
	var datos []model.Remxplaza
	err = json.Unmarshal(body, &datos)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// RECORRER datos para agregarlos a la tabla de Remxplaza.	
	fmt.Println("Remesa datos ID ", datos[0].Id)
	
	var totalDatos int = len (datos)
	fmt.Println("total datos", totalDatos)

	// Ciclo dentro de Remesas. Dentro de Tarjetas.
	for i := 0; i < totalDatos ; i++ {
		fmt.Println(datos[i] )


		 // var remxest model.RemxEst
		remxplaza := datos[i]
		//remxplaza.fecha = 

		err := model.UpdateRemxPlaza(remxplaza)
		if err != nil {
			c.JSON(500, err.Error())
			return
		}
	}
	c.JSON(200, "rem x est agregadas")

}

// Post Mnada Numrem
func CargarRemxPlaza(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var numrem model.Remxplaza
	err = json.Unmarshal(body, &numrem)
	fmt.Println(numrem)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	datos, err := model.CargarRemxPlaza(numrem.Numrem)
	if err != nil {
		c.JSON(200,  err.Error())
		return
	}

	c.JSON(200, datos)
}




//Post param (idremesa, estado, cant)
func AutoAsignarEstado (c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var remxplaza model.Remxplaza
	err = json.Unmarshal(body, &remxplaza)
	if err != nil {
		
		c.JSON(500, err.Error())
		return
	}
	fmt.Println("idremesa, estado y cant", remxplaza.Estado)

	// Mandar Procesar las Tarjetas con el estado mandado por parametro.
	 err = model.AutoAsignarEstado(remxplaza)
	if err != nil {
		c.JSON(200,  err.Error())
		return
	}

	c.JSON(200, "Se Actualizado a 16")

}


// err = model.UpdateRemxPlaza(remxplaza)
	// if err != nil {
	// 	c.JSON(500, err.Error())
	// 	return

	// }
	// c.JSON(200, "Actualizado correctamente")
