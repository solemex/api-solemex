// using SendGrid's Go Library
// https://github.com/sendgrid/sendgrid-go
package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	//"os"
	"github.com/gin-gonic/gin"
    "io/ioutil"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type Correo struct {
	Idusuariosweb string `db:"idusuariosweb"  json:"idusuariosweb"`
  To        string `db:"to"   	   json:"to"`
	Cuerpo    string `db:"cuerpo"  	 json:"cuerpo"`
	Subject   string `db:"subject"   json:"subject"`
  
}


func EnviarCorreos(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	fmt.Println(body)
	fmt.Println(err)

	var correos Correo
	err = json.Unmarshal(body, &correos)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	fmt.Println(correos.To)
	fmt.Println(correos.Subject)
	fmt.Println(correos.Cuerpo)


	// DE
	from := mail.NewEmail("Mensajería Solemex", "admin@md3.mx")
	subject := correos.Subject
	
	//PARA
	to := mail.NewEmail("Super Usuario", correos.To)
	plainTextContent := "and easy to do anywhere, even with Go"
	htmlContent := correos.Cuerpo
	message := mail.NewSingleEmail(from, subject, to,plainTextContent , htmlContent)

	client := sendgrid.NewSendClient("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")
	response, err := client.Send(message)
	if err != nil {
		log.Println(err)
	} else {
		fmt.Println(response.StatusCode)
		fmt.Println(response.Body)
		fmt.Println(response.Headers)
	}
}


// FUnacion para enviar un Template-

func CorreoTemplate(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
//	fmt.Println(body)
	
	var correos Correo
	err = json.Unmarshal(body, &correos)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// fmt.Println(correos.To)
	// fmt.Println(correos.Subject)
	

  m := mail.NewV3Mail()

  address := "admin@md3.mx"
  name := "Fabrica de Software Sait Md3"
  e := mail.NewEmail(name, address)

  m.SetFrom(e)
  fmt.Println("SetFrom",e)

  // Template Factura
  m.SetTemplateID("d-b2a71da28a464b12a111a654719a8f34")

  p := mail.NewPersonalization()


  // p.SetDynamicTemplateData("reset_code", "1234abcd")
  // p.To =  []*mail.Email{{ "Director", "manuel@sait.com.mx" } }


  tos := []*mail.Email{
    mail.NewEmail("Director", "manuel@sait.com.mx"),
 
  }

  p.AddTos(tos...)

  p.SetDynamicTemplateData("receipt", "true")
  p.SetDynamicTemplateData("total", "$ 12,500")

  items := []struct {
    text  string
    image string
    price string
  }{
    {"TIenda en Línea", "https://www.saitmd3.com/img/arts/tienda1.jpg", "$ 5,000."},
    {"Consultorias", "https://marketing-image-production.s3.amazonaws.com/uploads/3629f54390ead663d4eb7c53702e492de63299d7c5f7239efdc693b09b9b28c82c924225dcd8dcb65732d5ca7b7b753c5f17e056405bbd4596e4e63a96ae5018.png", "$ 10.90"},
    {"Blue Line Sneakers", "https://www.saitmd3.com/img/arts/indicadores.jpg", "$ 7,500.00"},
  }

  var itemList []map[string]string
  var item map[string]string
  for _, v := range items {
    item = make(map[string]string)
    item["text"] = v.text
    item["image"] = v.image
    item["price"] = v.price
    itemList = append(itemList, item)
  }
  p.SetDynamicTemplateData("items", itemList)

  p.SetDynamicTemplateData("name", "Manuel Gutierrez")
  p.SetDynamicTemplateData("address01", "Rio Guayalejo 143.")
  p.SetDynamicTemplateData("address02", "Jardines del Canada")
  p.SetDynamicTemplateData("city", "SAN ESCOBEDO")
  p.SetDynamicTemplateData("state", "NL")
  p.SetDynamicTemplateData("zip", "66050")

  m.AddPersonalizations(p)

  fmt.Println("AddPersonalizations", p)  
  request := sendgrid.GetRequest("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", "/v3/mail/send", "https://api.sendgrid.com")
  request.Method = "POST"
  
  var Body = mail.GetRequestBody(m)
  request.Body = Body
  
  //fmt.Println(request.Body)
  response, err := sendgrid.API(request)

  if err != nil {
    fmt.Println("error ", err)
  } else {
    fmt.Println(response.StatusCode)
    fmt.Println(response.Body)
    //fmt.Println(response.Headers)
  }  

}



// ACTIVAR CUENTA

func ActivarCuenta(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	
	var correos Correo
	err = json.Unmarshal(body, &correos)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}


	 fmt.Println(correos)
	 fmt.Println("idusuariosweb", correos.Idusuariosweb)

   // http://localhost:8080/activarusuario/20 	



  m := mail.NewV3Mail()

  address := "admin@md3.mx"
  name := "Activar Cuenta"
  e := mail.NewEmail(name, address)

  m.SetFrom(e)
  //fmt.Println("SetFrom",e)

  // Template Factura
  m.SetTemplateID("d-f0e836babb06431b8295807f250961be")

  p := mail.NewPersonalization()

  tos := []*mail.Email{
    mail.NewEmail("Director", correos.To),
 
  }

  p.AddTos(tos...)

  
  p.SetDynamicTemplateData("text", "Sistema de Mensajería") 
  p.SetDynamicTemplateData("c2a_button", "Activar Cuenta")
  p.SetDynamicTemplateData("header", "Fabrica de Software SAIT ERP")

 // cRutaActivar  := "http://localhost:8080/activarusuario/"+ correos.Idusuariosweb
  cRutaActivar  := "https://www.solemex.pro/plataforma/activarusuario/"+ correos.Idusuariosweb
  
  fmt.Println(cRutaActivar)
  p.SetDynamicTemplateData("c2a_link",  cRutaActivar)


  p.SetDynamicTemplateData("name", "Manuel Gutierrez")
  p.SetDynamicTemplateData("address01", "Rio Guayalejo 143.")
  p.SetDynamicTemplateData("address02", "Jardines del Canada")
  p.SetDynamicTemplateData("city", "SAN ESCOBEDO")
  p.SetDynamicTemplateData("state", "NL")
  p.SetDynamicTemplateData("zip", "66050")


  m.AddPersonalizations(p)

  fmt.Println("AddPersonalizations", p)  
  request := sendgrid.GetRequest("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", "/v3/mail/send", "https://api.sendgrid.com")
  request.Method = "POST"
  
  var Body = mail.GetRequestBody(m)
  request.Body = Body
  
  //fmt.Println(request.Body)
  response, err := sendgrid.API(request)

  if err != nil {
    fmt.Println("error ", err)
  } else {
    fmt.Println(response.StatusCode)
    fmt.Println(response.Body)
    //fmt.Println(response.Headers)
  }  

}





