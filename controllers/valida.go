package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

//Post param (idremesa  Regresa las tarjeras por validar /plaza)
func BusValidaRemxPlaza(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
	
		c.JSON(500, err.Error())
		return
	}

	var busvalida model.BusValida
	err = json.Unmarshal(body, &busvalida)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("BusValida ", busvalida.Idplaza)

		
	datos, err := model.BusValidaRemxPlaza(busvalida.Idremesa, busvalida.Estado)
	if err != nil {
		c.JSON(500, err.Error())
		return
	} else {
		c.JSON(200, datos)
	}
	
}

// Correctas
func Correctas (c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var correctas model.Correctas
	err = json.Unmarshal(body, &correctas)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("Correctas ", correctas.Numguia, correctas.Id)

	err2 := model.UpdateCorrectas(correctas)
	if err2 != nil {
			c.JSON(500, err2.Error())
			return
		} else {
			c.JSON(200, correctas)
		}

}

// Correctas
func Sobrantes (c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var sobrantes model.Sobrantes

	err = json.Unmarshal(body, &sobrantes)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("Sobrantes ", sobrantes.Numcli)

	err2 := model.AddSobrante(sobrantes)
	if err2 != nil {
		c.JSON(500, err2.Error())
		return
	} else {
		c.JSON(200, "Add Sobrante OK")
	}

}


//Faltantes POST Diferencias
func Faltantes (c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var faltantes []model.Faltantes

	err = json.Unmarshal(body, &faltantes)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	fmt.Println("Faltantes ", faltantes)

	var totalFal int = len (faltantes)
	fmt.Println("total Faltantes", totalFal)

	// Ciclo dentro de Remesas. Dentro de Tarjetas.
	for i := 0; i < totalFal ; i++ {

		fmt.Println(faltantes[i] )
		 // var remxest model.RemxEst
		Faltante  := faltantes[i]

		err := model.AddFaltante(Faltante)
		if err != nil {
			c.JSON(500, err.Error())
			return
		}
	}
	c.JSON(200, "Faltantes agregadas Correctamente")


}


