package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

func PendxMensajero(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.Conapp() //cattarjetas PendxMensajero
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// Validar Idmensajero recibida
	var pendxm model.PendxM
	err = json.Unmarshal(body, &pendxm)
	fmt.Println(pendxm.Idmensajero)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// Hacer consulta
	datos, err := model.PendxMensajero(pendxm.Idmensajero)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	c.JSON(200, datos)
}

func PrioridadxMens(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.Conapp() //cattarjetas PrioridadxMens
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// Validar Idmensajero recibida
	var priorm model.PriorxM
	err = json.Unmarshal(body, &priorm)
	//fmt.Println(priorm.Idmensajero)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// Hacer consulta
	datos, err := model.PrioridadxMens(priorm.Idmensajero)
	if err != nil {
		c.JSON(200, err.Error())
		return
	}

	c.JSON(200, datos)

}

func GetSuc(c *gin.Context) { //OBTENER tarjetas
	id := c.Param("id")
	sucursal, err := model.GetSuc(id) //tarjetas gettarjetas
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, sucursal)
}
