package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
	// "strconv"
)

func GetSuperInt(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var super model.SupervisionInt
	err = json.Unmarshal(body, &super)

	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	datos, err := model.GetSuperInt(super.Mensajero, super.Fecha)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}