package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

func ListNiveles(c *gin.Context) {
	niveles, err := model.ListNiveles()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, niveles)
}

func GetNiveles(c *gin.Context) {
	id := c.Param("id")
	niveles, err := model.GetNiveles(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, niveles)
}

func InsertNiveles(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var niveles model.Niveles
	err = json.Unmarshal(body, &niveles)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertNiveles(niveles)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}

func UpdateNiveles(c *gin.Context) {
	id := c.Param("id")
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var niveles model.Niveles
	err = json.Unmarshal(body, &niveles)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	niveles.Id = id
	err = model.UpdateNiveles(niveles)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}

func DeleteNiveles(c *gin.Context) {
	id := c.Param("id")
	err := model.DeleteNiveles(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Eliminado correctamente")
}
