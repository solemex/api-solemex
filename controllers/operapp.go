package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

func Entregar(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.Conapp() //cattarjetas PrioridadxMens
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// Validar Parentesco, Tipoid, Numid, Tipo_entrega recibidos
	var entregarid model.EntregarId

	err = json.Unmarshal(body, &entregarid)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	//  Actualizar Datos de Id.
	err = model.UpdateEntrega(entregarid)
	if err != nil {
		c.JSON(200, err.Error())
		return
	}

	//Poner aquí actualiza rem
	fmt.Println("id remesa",entregarid.Idrem)
	_,err = model.ActualizaRem(entregarid.Idrem) 
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	c.JSON(200, "Actualizado con exito")

}

func ActualizaEntrega(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	// contarjetas, err := model.Conapp() //cattarjetas PrioridadxMens
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// Validar Parentesco, Tipoid, Numid, Tipo_entrega recibidos
	// var entregarid model.EntregarId
	var entregarid model.ActualizaEntregaE

	err = json.Unmarshal(body, &entregarid)
	fmt.Println("Numguia",entregarid.Numguia)
	fmt.Println("Estatus",entregarid.Estatus)
	fmt.Println("Nomestatus",entregarid.Nomestatus)
	fmt.Println("id remesa",entregarid.Idrem)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	//  Actualizar Datos de Id.
	err = model.ActualizaEstatus(entregarid)
	if err != nil {
		c.JSON(200, err.Error())
		return
	}
	fmt.Println("id remesa, actualiza entrega",entregarid.Idrem)
	fmt.Println("Todos los datos, actualiza entrega",entregarid)
	//Poner aquí actualiza rem
	_,err = model.ActualizaRem(entregarid.Idrem) 
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	c.JSON(200, "Actualizado con exito")

}



func AddIntento(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var addint model.AddInt
	err = json.Unmarshal(body, &addint)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	
	err = model.AddIntentos(addint)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	err = model.UpdateIntento(addint)
	fmt.Println(addint)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	
	fmt.Println("id remesa, AddIntentos", addint.Idrem)
	fmt.Println("todos los datos, AddIntentos", addint)
	//Poner aquí actualiza rem
	_,err = model.ActualizaRem(addint.Idrem) 
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	
	c.JSON(200, "Insertado correctamente")



}

