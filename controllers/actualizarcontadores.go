package controllers

import (
	"gitlab.com/solemex/api-solemex/model"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
)




func ActualizaRemesa(c *gin.Context) { 
	id := c.Param("id")

	nid, err1 := strconv.Atoi(id)
	if err1 == nil {
		fmt.Println(nid)
	}


	clientes, err := model.ActualizaRem(nid) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, clientes)
}
