package controllers


import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)

//Reporte de Salida
func ArchivoSal(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var archivosalida model.ArchivoSalida
	// Validar entrada
	err = json.Unmarshal(body, &archivosalida)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	// mandar consultar
	datos, err := model.ArchivoSal(archivosalida.Numrem)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)

}
