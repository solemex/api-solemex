package controllers

import (
	"gitlab.com/solemex/api-solemex/model"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"fmt"
)


func ListGuias(c *gin.Context) {
	guias, err := model.ListGuias() 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, guias)
}

func GetGuias(c *gin.Context) { 
	id := c.Param("id")
	guias, err := model.GetGuias(id) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, guias)
}


func GetGuiasRem(c *gin.Context) { 
	idremesa := c.Param("id")
	guias, err := model.GetGuiasRem(idremesa) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, guias)
}



	// POST Guias
func InsertGuias(c *gin.Context) { 
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var guias model.Guias
	err = json.Unmarshal(body, &guias)

	fmt.Println("folio " , guias.Numrem)

	if guias.Numrem =="" {
		c.JSON(400, "Guia folio  No Existe." + guias.Numrem)
		return
	}

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// LLamar a función de Grabar Guias
	err = model.InsertGuias(guias)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
   
	c.JSON(200, "Guias Agregado con Exito" )
}


// func UpdateCatServicios(c *gin.Context) {
// 	id := c.Param("id")                         //USA  PUT
// 	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		return
// 	}
// 	var servicios model.Servicios
// 	err = json.Unmarshal(body, &servicios)
// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		return
// 	}
// 	servicios.ID = id
// 	err = model.UpdateServicios(servicios)
// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		return

// 	}
// 	c.JSON(200, "Actualizado correctamente")
// }
