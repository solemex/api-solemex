package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)


func ListSucursalesxCli(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var idcli model.SucxCli
	err = json.Unmarshal(body, &idcli)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// fmt.Println("ID CLIENTE ",idcli.Idcliente)
	id := idcli.Idcliente
	datos, err := model.ListSucursalesxCli(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)
}

func ListSucursales(c *gin.Context) {
	sucursales, err := model.ListSucursales() //sucursales listsucursales
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, sucursales)
}

func ListCatSuc(c *gin.Context) {
	sucursales, err := model.ListCatSuc() //sucursales listsucursales
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, sucursales)
}

func SucxNumsuc(c *gin.Context) { //OBTENER sucursales
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var suc model.SucxCli
	err = json.Unmarshal(body, &suc)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// fmt.Println("ID CLIENTE ",suc.sucente)
	id := suc.Idcliente
	numsuc := suc.Numsuc
	datos, err := model.SucxNumsuc(id, numsuc)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, datos)

}


func GetSucursales(c *gin.Context) { //OBTENER sucursales
	id := c.Param("id")
	sucursales, err := model.GetSucursales(id) //sucursales getsucursales
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, sucursales)
}

func InsertSucursales(c *gin.Context) { //INSERTA A CLIENTE
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var sucursales model.NewSucursal
	err = json.Unmarshal(body, &sucursales)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertSucursales(sucursales)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}

func UpdateSucursales(c *gin.Context) {
	id := c.Param("id")                         //USA  PUT
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var sucursales model.Sucursales
	err = json.Unmarshal(body, &sucursales)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	sucursales.Id = id
	err = model.UpdateSucursales(sucursales)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}

func DeleteSucursales(c *gin.Context) {
	id := c.Param("id")
	err := model.DeleteSucursales(id)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, "Eliminado correctamente")
}
