//Controlador Diferencias

package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
	// "fmt"
)

func ListDiferencias(c *gin.Context) {
	diferencias, err := model.ListDiferencias()
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, diferencias)
}

func AddDiferencias(c *gin.Context) { //INSERTA A CLIENTE
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var diferencias model.Diferencias
	err = json.Unmarshal(body, &diferencias)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.AddDiferencias(diferencias)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}


func MarcarDiferencias(c *gin.Context) { 
	// fmt.Println("MarcarDiferencias")
	id := c.Param("id")

	Adicionales, err := model.MarcarDiferencias(id) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	ntotaldif := len(Adicionales)


	// Ciclo dentro de Remesas. Dentro de Tarjetas.
	for i := 0; i < ntotaldif; i++ {

		nIdAdicional:= Adicionales[i].Id
		// fmt.Println(Adicionales[i].Numcli)
		// fmt.Println(Adicionales[i].Idremesa)
		cNumcli := "%" + Adicionales[i].Numcli + "%"
	   

		numguiaArr ,err := model.FindNumcli(cNumcli , Adicionales[i].Idremesa )
		if err != nil {
			c.JSON(500, err.Error())
			return
		}

		if numguiaArr.Numguia != "" {
			// fmt.Println(numguiaArr, nIdAdicional)

			numguiaArr.Id = nIdAdicional
			err2 := model.UpdateAdicional(numguiaArr)
			if err2 != nil {
				c.JSON(500, err2.Error())
				return

			}
		} 

	}


	c.JSON(200, Adicionales)
}