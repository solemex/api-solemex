package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/solemex/api-solemex/model"
	"io/ioutil"
)



// Obtener el Email de un ID en Tarjetas
func GetEmailxId(c *gin.Context) { 
	id := c.Param("id")
	
	email, err := model.GetEmailxId(id) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, email)
}



func UpdateAsignar(c *gin.Context) {

	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		fmt.Println("Err =", err)
		c.JSON(500, err.Error())
		return
	}

	var asignar model.Asignar
	err = json.Unmarshal(body, &asignar)
	if err != nil {
		fmt.Println("Err =", err)
		c.JSON(500, err.Error())
		return
	}

	//Mandar llamar
	var Numguia1 = asignar.Numguia
	var Numcli1  = asignar.Numcli

	fmt.Println("BuscarAsignar ", Numguia1, Numcli1)

	idkey, err := model.BusIdAsignar(Numguia1)

	
	fmt.Println("idkey" , idkey.Id, idkey.Idremesa)


	asignar.Id = idkey.Id
	fmt.Println(asignar)

	// Actualizar Tarjetas Modo Asignar.
	if idkey.Id > 0 {
		
		err2 := model.UpdateAsignar(asignar)
		if err2 != nil {
			c.JSON(500, err2.Error())
			return
		} else {

			// Metodo para incrementar contadores
			datos, err3 := model.ActualizaRem(idkey.Idremesa)
			if err3 != nil {
				c.JSON(500, err3.Error())
				return
			} else {
				fmt.Println(datos)
			}

			c.JSON(200, asignar)
		}
	}	

}
