package controllers

import (
	"gitlab.com/solemex/api-solemex/model"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"fmt"
)


func ListPreGuias(c *gin.Context) {
	preguias, err := model.ListPreGuias() 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, preguias)
}

func GetPreGuias(c *gin.Context) { 
	id := c.Param("id")
	preguias, err := model.GetPreGuias(id) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, preguias)
}


func GetPreGuiasRem(c *gin.Context) { 
	idremesa := c.Param("id")
	preguias, err := model.GetPreGuiasRem(idremesa) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, preguias)
}

	// POST Guias
func InsertPreGuias(c *gin.Context) { 
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var preguias model.PreGuias
	err = json.Unmarshal(body, &preguias)

	fmt.Println("folio " , preguias.Numrem)

	if preguias.Numrem =="" {
		c.JSON(400, "Guia folio  No Existe." + preguias.Numrem)
		return
	}

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// LLamar a función de Grabar Guias
	err = model.InsertPreGuias(preguias)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
   
	c.JSON(200, "Guias Agregado con Exito" )
}


// func UpdateCatServicios(c *gin.Context) {
// 	id := c.Param("id")                         //USA  PUT
// 	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		return
// 	}
// 	var servicios model.Servicios
// 	err = json.Unmarshal(body, &servicios)
// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		return
// 	}
// 	servicios.ID = id
// 	err = model.UpdateServicios(servicios)
// 	if err != nil {
// 		c.JSON(500, err.Error())
// 		return

// 	}
// 	c.JSON(200, "Actualizado correctamente")
// }
