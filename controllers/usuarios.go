package controllers

import (
	"gitlab.com/solemex/api-solemex/model"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)

func ListCatUsuarios(c *gin.Context) {
	usuarios, err := model.ListCatUsuarios() //usuarios listusuarios
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, usuarios)
}

func GetCatUsuarios(c *gin.Context) { //OBTENER usuarios
	id := c.Param("id")
	usuarios, err := model.GetCatUsuarios(id) //usuarios getusuarios
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, usuarios)
}

func InsertCatUsuarios(c *gin.Context) { //INSERTA A CLIENTE
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var usuarios model.Usuarios
	err = json.Unmarshal(body, &usuarios)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	err = model.InsertCatUsuarios(usuarios)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Insertado correctamente")
}

func UpdateCatUsuarios(c *gin.Context) {
	id := c.Param("id")                         //USA  PUT
	body, err := ioutil.ReadAll(c.Request.Body) //SIRVE PARA LEER EL BODY
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	var usuarios model.Usuarios
	err = json.Unmarshal(body, &usuarios)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	usuarios.ID = id
	err = model.UpdateCatUsuarios(usuarios)
	if err != nil {
		c.JSON(500, err.Error())
		return

	}
	c.JSON(200, "Actualizado correctamente")
}
