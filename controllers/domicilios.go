package controllers

import (
	"gitlab.com/solemex/api-solemex/model"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"fmt"
)


func ListDomicilios(c *gin.Context) {
	domicilios, err := model.ListDomicilios() 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, domicilios)
}

//GET DOMICILIOS
func GetDomicilios(c *gin.Context) { 
	id := c.Param("id")
	domicilios, err := model.GetDomicilios(id) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}
	c.JSON(200, domicilios)
}

	// POST DOMICILIOS
func InsertDomicilios(c *gin.Context) { 
	body, err := ioutil.ReadAll(c.Request.Body) 
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	var domicilios model.Domicilios
	err = json.Unmarshal(body, &domicilios)

	fmt.Println("direccion  " , domicilios.Direccion)

	if domicilios.Direccion =="" {
		c.JSON(400, "Direccion No Existe.")
		return
	}

	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	err = model.InsertDomicilios(domicilios)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	// // LLAMAR IDget
	// iddom, err2 := model.Getiddom ()
	// if err2 != nil {
	// 	c.JSON(500, err2.Error())
	// 	return
	// }
	// c.JSON(200, iddom)
}

