package modeldb

import (
	 "fmt"
 	_ "github.com/go-sql-driver/mysql"
 	"github.com/jmoiron/sqlx"
 )


var DB *sqlx.DB

func OpenDB() {
	fmt.Println("Manuel was here")
	// Local
	//db, err := sqlx.Open("mysql", "root:@tcp(localhost:3306)/bajopuerta")
	// soportesait.com Pruebas. 
	db, err := sqlx.Open("mysql", "md3admin:DelSarmiento.171@tcp(soportesait.com:3306)/BajoPuerta")
	
	// Real de Solemex
	//db, err := sqlx.Open("mysql", "md3admin:DelSarmiento.171@tcp(soportesait:3306)/BajoPuerta")


	if err != nil {
		fmt.Println(err)
		return
	}
	DB = db
	err = DB.Ping()
	if err != nil {
		fmt.Println(err)
		return
	}
}

func GetDb() *sqlx.DB {
	return DB
}