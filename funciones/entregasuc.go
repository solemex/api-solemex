// using SendGrid's Go Library
// https://github.com/sendgrid/sendgrid-go
package funciones

import (
	"encoding/json"
	"fmt"
	"log"
	//"os"
	"github.com/gin-gonic/gin"
    "io/ioutil"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type Entregasuc struct {
	Idusuariosweb string `db:"idusuariosweb"  json:"idusuariosweb"`
  Email     string   `db:"email"   	 json:"email"`
	Cuerpo    string   `db:"cuerpo"  	 json:"cuerpo"`
	Subject   string   `db:"subject"   json:"subject"`
	Numguia   string   `db:"numguia"   json:"numguia"`
  Fecha     string    `db:"fecha"   json:"fecha"`
  Hora      string    `db:"hora"   json:"hora"`
  Nomsuc    string    `db:"nomsuc"   json:"nomsuc"`
}

// ACTIVAR CUENTA
func SendEntregaSuc (c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)	
	var entregasuc Entregasuc
	err = json.Unmarshal(body, &entregasuc)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	 fmt.Println(entregasuc)
	 fmt.Println("numguia", entregasuc.Numguia)

   // http://localhost:8080/activarusuario/20 	

  m := mail.NewV3Mail()

  address := "admin@md3.mx"
  name := "Entrega en Sucursal"
  e := mail.NewEmail(name, address)

  m.SetFrom(e)
  //fmt.Println("SetFrom",e)

  // Template Entregaexitosa en SucurSal
  m.SetTemplateID("d-96ecffa607fb4726becaa4aea9170456")

  p := mail.NewPersonalization()

  tos := []*mail.Email{
    mail.NewEmail("Director", entregasuc.Email),
 
  }
  p.AddTos(tos...)
  p.SetDynamicTemplateData("text", "Para cualquier duda o aclaración comunicate al Centro de Atención a Clientes Banregio 01-81-BANREGIO (22-673-446)")
  p.SetDynamicTemplateData("c2a_button", "Más detalles aquí")
  p.SetDynamicTemplateData("header", "Fabrica de Software SAIT ERP")

  cNumguia := entregasuc.Numguia
  p.SetDynamicTemplateData("numguia", cNumguia)
  p.SetDynamicTemplateData("header", "Número de Guía:")

  //logo Banco
  p.SetDynamicTemplateData("logourl","https://marketing-image-production.s3.amazonaws.com/uploads/84a8c1f7f71efeef9fe471fceffcd8c8d4255ed5a060c0d4216c3cd475621ceae84868d8a0b8fd433951bffa86fe576a0830c135e7e99363f096359e3b083599.png")

  p.SetDynamicTemplateData("infoa1", "Tu Tarjeta Banregio ya se encuentra en sucursal ")
  p.SetDynamicTemplateData("infoa2", "Acabamos de entregar tu tarjeta en sucursal :")

  cFecha:= entregasuc.Fecha
  p.SetDynamicTemplateData("fecha", cFecha)

  cHora := entregasuc.Hora
  p.SetDynamicTemplateData("hora", cHora)

  p.SetDynamicTemplateData("link_sucursales", "https://www.banregio.com/oficinas_lista.php")

  cNomsuc := entregasuc.Nomsuc
  p.SetDynamicTemplateData("nomsuc", cNomsuc)


  cRutaActivar  := "https://www.solemex.pro/plataforma/linkentregasuc/"+ entregasuc.Numguia
  

  fmt.Println(cRutaActivar)
  p.SetDynamicTemplateData("c2a_link",  cRutaActivar)

  p.SetDynamicTemplateData("name", "Manuel Gutierrez")
  p.SetDynamicTemplateData("address01", "Rio Guayalejo 143.")
  p.SetDynamicTemplateData("address02", "Jardines del Canada")
  p.SetDynamicTemplateData("city", "SAN ESCOBEDO")
  p.SetDynamicTemplateData("state", "NL")
  p.SetDynamicTemplateData("zip", "66050")

  m.AddPersonalizations(p)

  fmt.Println("AddPersonalizations", p)  
  request := sendgrid.GetRequest("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", "/v3/mail/send", "https://api.sendgrid.com")
  request.Method = "POST"
  
  var Body = mail.GetRequestBody(m)
  request.Body = Body
  
  //fmt.Println(request.Body)
  response, err := sendgrid.API(request)

  if err != nil {
    log.Println("error ", err)
  } else {
    fmt.Println(response.StatusCode)
    fmt.Println(response.Body)
    //fmt.Println(response.Headers)
  }  

}

