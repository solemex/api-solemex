// using SendGrid's Go Library
// https://github.com/sendgrid/sendgrid-go
package funciones

import (
	"encoding/json"
	"fmt"
	"log"
	//"os"
	"github.com/gin-gonic/gin"
    "io/ioutil"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type Entrega struct {
	Idusuariosweb string `db:"idusuariosweb"  json:"idusuariosweb"`
  Email     string   `db:"email"   	   json:"email"`
	Cuerpo    string   `db:"cuerpo"  	   json:"cuerpo"`
	Subject   string   `db:"subject"     json:"subject"`
	Numguia   string   `db:"numguia"     json:"numguia"`
  Nomrecibe string   `db:"nomrecibe"   json:"nomrecibe"`
  Fecha     string   `db:"fecha"       json:"fecha"`
  Hora      string   `db:"hora"        json:"hora"`
}

// ACTIVAR CUENTA
func SendEntrega (c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)	
	var entrega Entrega 
	err = json.Unmarshal(body, &entrega)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

//	 fmt.Println(entrega)
//	 fmt.Println("numguia", entrega.Numguia)

   // http://localhost:8080/activarusuario/20 	

  m := mail.NewV3Mail()

  address := "admin@md3.mx"
  name := "Entrega Exitosa"
  e := mail.NewEmail(name, address)

  m.SetFrom(e)
  //fmt.Println("SetFrom",e)

  // Template Entregaexitosa en domicilio
  m.SetTemplateID("d-0ba2e39d59c542099a2a52bb962ed97d")

  p := mail.NewPersonalization()

  tos := []*mail.Email{
    mail.NewEmail("Director", entrega.Email),
 
  }
  p.AddTos(tos...)
  p.SetDynamicTemplateData("c2a_button", "Tarjeta Entregada")
  p.SetDynamicTemplateData("header", "Número de Guía: ")

  cNumguia := entrega.Numguia
  p.SetDynamicTemplateData("numguia", cNumguia)

  p.SetDynamicTemplateData("infoa1", "!Tu Tarjeta Banregio fue entregada!")
  p.SetDynamicTemplateData("infoa2", "Entregamos tu tarjeta Banregio en el domicilio que nos indicaste :")

  // Logo del Cliente. (debe estar en SendGrid)
  p.SetDynamicTemplateData("logourl" , "https://marketing-image-production.s3.amazonaws.com/uploads/84a8c1f7f71efeef9fe471fceffcd8c8d4255ed5a060c0d4216c3cd475621ceae84868d8a0b8fd433951bffa86fe576a0830c135e7e99363f096359e3b083599.png")


  cNomrecibe := entrega.Nomrecibe
  p.SetDynamicTemplateData("nomrecibe", cNomrecibe)

  cFecha := entrega.Fecha
  p.SetDynamicTemplateData("fecha", cFecha)

  cHora := entrega.Hora
  p.SetDynamicTemplateData("hora", cHora)



  cRutaActivar  := "https://www.solemex.pro/plataforma/linkrastreo/"+ entrega.Numguia
  
  p.SetDynamicTemplateData("text", "Para cualquier duda o aclaración comunicate al Centro de Atención a Clientes Banregio 01-81-BANREGIO (22-673-446)")


//  fmt.Println(cRutaActivar)
  p.SetDynamicTemplateData("c2a_link",  cRutaActivar)

  p.SetDynamicTemplateData("name", "Manuel Gutierrez")
  p.SetDynamicTemplateData("address01", "Rio Guayalejo 143.")
  p.SetDynamicTemplateData("address02", "Jardines del Canada")
  p.SetDynamicTemplateData("city", "SAN ESCOBEDO")
  p.SetDynamicTemplateData("state", "NL")
  p.SetDynamicTemplateData("zip", "66050")

  m.AddPersonalizations(p)

//  fmt.Println("AddPersonalizations", p)  
  request := sendgrid.GetRequest("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", "/v3/mail/send", "https://api.sendgrid.com")
  request.Method = "POST"
  
  var Body = mail.GetRequestBody(m)
  request.Body = Body
  
  //fmt.Println(request.Body)
  response, err := sendgrid.API(request)

  if err != nil {
    log.Println("error ", err)
  } else {
    fmt.Println(response.StatusCode)
    fmt.Println(response.Body)
    //fmt.Println(response.Headers)
  }  

}
