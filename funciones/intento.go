// using SendGrid's Go Library
// https://github.com/sendgrid/sendgrid-go
package funciones

import (
	"encoding/json"
	"fmt"
	"log"
	//"os"
	"github.com/gin-gonic/gin"
    "io/ioutil"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

type Intento struct {
	Idusuariosweb string `db:"idusuariosweb"  json:"idusuariosweb"`
  Email       string `db:"email"   	 json:"email"`
	Cuerpo     string `db:"cuerpo"  	 json:"cuerpo"`
	Subject    string `db:"subject"   json:"subject"`
	Numguia    string   `db:"numguia"   json:"numguia"`
  Motivo string `db:"motivo"   json:"motivo"`
  Fecha  string `db:"fecha"   json:"fecha"`
  Hora string `db:"hora"   json:"hora"`
  longitude string `db:"longitude"   json:"longitude"`
  latitude string `db:"latitude"   json:"latitude"`
}



// ACTIVAR CUENTA
func SendIntento (c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)	
	var intento Intento 
	err = json.Unmarshal(body, &intento)
	if err != nil {
		c.JSON(500, err.Error())
		return
	}

	 fmt.Println(intento)
	 fmt.Println("numguia", intento.Numguia)

   // http://localhost:8080/activarusuario/20 	

  m := mail.NewV3Mail()

  address := "admin@md3.mx"
  name := "Intento de Entrega"
  e := mail.NewEmail(name, address)

  m.SetFrom(e)
  //fmt.Println("SetFrom",e)

  // Template intento de Entrega

  m.SetTemplateID("d-4d4b8b28ac4d49499981d2a24a3920b0")

  p := mail.NewPersonalization()

  tos := []*mail.Email{
    mail.NewEmail("Director", intento.Email),
 
  }
  p.AddTos(tos...)
  p.SetDynamicTemplateData("text", "Para cualquier duda o aclaración comunicate al Centro de Atención a Clientes Banregio 01-81-BANREGIO (22-673-446)")
  p.SetDynamicTemplateData("c2a_button", "Intento de Entrega")
  p.SetDynamicTemplateData("header", "Número de Guía:")

  cNumguia := intento.Numguia
  p.SetDynamicTemplateData("numguia", cNumguia)

  // Logo del Cliente. (debe estar en SendGrid)
  p.SetDynamicTemplateData("logourl" , "https://marketing-image-production.s3.amazonaws.com/uploads/84a8c1f7f71efeef9fe471fceffcd8c8d4255ed5a060c0d4216c3cd475621ceae84868d8a0b8fd433951bffa86fe576a0830c135e7e99363f096359e3b083599.png")

  cMotivo := intento.Motivo
  p.SetDynamicTemplateData("motivo", cMotivo)

  cFecha := intento.Fecha
  p.SetDynamicTemplateData("fecha", cFecha)

  cHora := intento.Hora
  p.SetDynamicTemplateData("hora", cHora)

  //leyendas Fijas
  p.SetDynamicTemplateData("infoIn2", "Tratamos de localizarte en el domicilio que nos indicaste.")
  p.SetDynamicTemplateData("infoIn3", "Siguiente paso: ")


  nNumIntentos := 2
  if nNumIntentos > 2 {

    // Si el intenro es mayor a 2  (se se va asucursal)
    p.SetDynamicTemplateData("infoIn4", "")
    p.SetDynamicTemplateData("infoIn5", "Devolución a Sucursal")
    
  } else {
    // Si el intenro es menor a 2
    p.SetDynamicTemplateData("infoIn4", "Visita")
    p.SetDynamicTemplateData("infoIn5", "")

  }













  //cRutaActivar  := "http://localhost:8081/linkrastreo/"+ intento.Numguia  
  cRutaActivar  := "https://www.solemex.pro/plataforma/linkintentos/"+ intento.Numguia
  
  fmt.Println(cRutaActivar)
  p.SetDynamicTemplateData("c2a_link",  cRutaActivar)

  p.SetDynamicTemplateData("name", "Manuel Gutierrez")
  p.SetDynamicTemplateData("address01", "Rio Guayalejo 143.")
  p.SetDynamicTemplateData("address02", "Jardines del Canada")
  p.SetDynamicTemplateData("city", "SAN ESCOBEDO")
  p.SetDynamicTemplateData("state", "NL")
  p.SetDynamicTemplateData("zip", "66050")

  m.AddPersonalizations(p)

  fmt.Println("AddPersonalizations", p)  
  request := sendgrid.GetRequest("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", "/v3/mail/send", "https://api.sendgrid.com")
  request.Method = "POST"
  
  var Body = mail.GetRequestBody(m)
  request.Body = Body
  
  //fmt.Println(request.Body)
  response, err := sendgrid.API(request)

  if err != nil {
    log.Println("error ", err)
  } else {
    fmt.Println(response.StatusCode)
    fmt.Println(response.Body)
    //fmt.Println(response.Headers)
  }  

}
