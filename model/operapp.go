package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type EntregarId struct {
	Id            int    `db:"id"    	  		json:"id"`
	Latitude      string `db:"latitude"   		json:"latitude"`
	Longitude     string `db:"longitude"  		json:"longitude"`
	Nomrecibe     string `db:"nomrecibe"  		json:"nomrecibe"`
	Parentesco    string `db:"parentesco"   	json:"parentesco"`
	Tipoid        string `db:"tipoid"    		json:"tipoid"`
	Numid         string `db:"numid"  			json:"numid"`
	Fecha_entrega string `db:"fecha_entrega"   	json:"fecha_entrega"` //DATE
	Estatus       int    `db:"estatus"   		json:"estatus"`
	Nomestatus    string `db:"nomestatus"   	json:"nomestatus"`
	Hora_entrega  string `db:"hora_entrega"   	json:"hora_entrega"`
	Tipo_entrega  int    `db:"tipo_entrega"	  	json:"tipo_entrega"`
	Id_sucursal   int    `db:"id_sucursal"    	json:"id_sucursal"`
	Idrem         int    `db:"idremesa"    	    json:"idremesa"`
}

type ActualizaEntregaE struct {
	Numguia       string    `db:"numguia"    	  json:"numguia"`
	Estatus       int       `db:"estatus"   	  json:"estatus"`
	Nomestatus    string    `db:"nomestatus"   	  json:"nomestatus"`
	Tipoent       string    `db:"tipoent"   	  json:"tipoent"`
	Idrem         int       `db:"idremesa"   	  json:"idremesa"`
}

type AddInt struct {
	Id           int    `db:"id"    	  		  json:"id"`
	Numguia      string `db:"numguia"   		  json:"numguia"`
	Num_intentos int    `db:"num_intentos"  	  json:"num_intentos"`
	Latitude     string `db:"latitude"   		  json:"latitude"`
	Longitude    string `db:"longitude"  		  json:"longitude"`
	Motivo       string `db:"motivo"    		  json:"motivo"`
	Comentarios  string `db:"comentarios"  	      json:"comentarios"`
	Fecha        string `db:"fecha"   			  json:"fecha"` //DATE
	Hora         string `db:"hora"   			  json:"hora"`
	Mensajero    int    `db:"mensajero"   		  json:"mensajero"`
	Remesa       string `db:"remesa"   			  json:"remesa"`
	Idrem        int    `db:"idremesa"   	      json:"idremesa"`

}


func UpdateEntrega(entregarid EntregarId) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE tarjetas
		SET latitude=:latitude, longitude=:longitude, nomrecibe=:nomrecibe,
			 parentesco=:parentesco, tipoid=:tipoid,  numid=:numid,
			fecha_entrega=:fecha_entrega, estatus=:estatus, nomestatus=:nomestatus,
			hora_entrega=:hora_entrega, tipo_entrega=:tipo_entrega,
			id_sucursal=:id_sucursal
		WHERE id=:id`, entregarid)
	return
}

func ActualizaEstatus(entregarid ActualizaEntregaE) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE tarjetas
		SET estatus=:estatus, nomestatus=:nomestatus,  tipoent=:tipoent
		WHERE numguia=:numguia`, entregarid)
	return
}





func AddIntentos(addint AddInt) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO intentos (numguia, remesa, num_intentos, latitude,
			   longitude, motivo, comentarios,fecha, hora, mensajero )
		VALUES(:numguia, :remesa, :num_intentos, :latitude, :longitude, :motivo, :comentarios,
		       :fecha, :hora, :mensajero)`, &addint)
	return
}



func UpdateIntento(addint AddInt) (err error) {
	var DB = modeldb.GetDb()

	_, err = DB.NamedExec(`UPDATE tarjetas
		SET num_intentos=:num_intentos
		WHERE numguia=:numguia and id> 1`, addint)
	return
}




