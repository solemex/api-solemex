package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

//Reporte de salida
type ArchivoSalida struct {
	Id            int    `db:"id"   	   	 	 json:"id"`
	Nomcli        string `db:"nomcli"   	   	 json:"nomcli"`
	Numcli        string `db:"numcli"   	   	 json:"numcli"`
	Numrem        string `db:"numrem"   	   	 json:"numrem"`
	Codigo        string `db:"codigo"   	   	 json:"codigo"`
	Tipotarjeta   string `db:"tipotarjeta"   	 json:"tipotarjeta"`
	Fecha         string `db:"fecha"   	    	 json:"fecha"`
	Hora          string `db:"hora"   	    	 json:"hora"`
	Fecha_entrega string `db:"fecha_entrega"   	 json:"fecha_entrega"`
	Motivo        string `db:"motivo"   	     json:"motivo"`
	Nomrecibe     string `db:"nomrecibe"   	     json:"nomrecibe"`
	Ciudad        string `db:"ciudad"   	     json:"ciudad"`
}

func ArchivoSal(numrem string) (archivosalida []ArchivoSalida, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&archivosalida, `SELECT COALESCE(tarjetas.nomcli,'') as nomcli,
	 COALESCE(tarjetas.numcli,'') as numcli,  COALESCE(tarjetas.numrem, '') as numrem,
     COALESCE(tarjetas.numcli,'') as numcli,  COALESCE(tarjetas.codigo,'') as codigo,
     COALESCE(tarjetas.tipotarjeta,'') as tipotarjeta,  COALESCE(intentos.fecha,'') as fecha,
     COALESCE(intentos.hora,'') as hora, COALESCE(tarjetas.fecha_entrega,'') as fecha_entrega,
     COALESCE(intentos.motivo,'') as motivo, COALESCE(tarjetas.nomrecibe,'') as nomrecibe,
     tarjetas.ciudad
	FROM tarjetas
	LEFT JOIN intentos ON intentos.numguia = tarjetas.numguia
    WHERE tarjetas.numrem=?`, numrem)
	return
}
