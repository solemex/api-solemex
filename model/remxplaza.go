package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type Remxplaza struct {
	Id         int    `db:"id"     	 	   json:"id"`
	Idremesa   int    `db:"idremesa"       json:"idremesa"`
	Numrem     string `db:"numrem"	 	   json:"numrem"`
	Estado     string `db:"estado"     	   json:"estado"`
	Cant       int    `db:"cant"           json:"cant"`
	Guiamens   string `db:"guiamens"  	   json:"guiamens"`
	Mensajeria string `db:"mensajeria"     json:"mensajeria"`
	Fecha      string `db:"fecha"     	   json:"fecha"` 
	Hora       string `db:"hora"       	   json:"hora"` 
	Idplaza    int    `db:"idplaza"        json:"idplaza"`
}


type RemxEst struct {
	Idremesa   int   `db:"idremesa"       json:"idremesa"`
	Numrem     string `db:"numrem"	 	   json:"numrem"`
	Estado     string `db:"estado"     	   json:"estado"`
	Cant       int    `db:"cant"           json:"cant"`
}


type AsignarEstado struct {
	idremesa 	int 
	Estado   	string
}




// Procear o Actualizar Tabla 
func UpdateRemxPlaza(remxplaza Remxplaza) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE remxplaza
		SET guiamens=:guiamens, mensajeria=:mensajeria,
			fecha="2019-05-20", hora=:hora
		WHERE id=:id`, remxplaza)
	fmt.Println(remxplaza.Id)
	return
}




func GetCantxEstado(idRemesa int) (remxest []RemxEst, err error){
	var DB = modeldb.GetDb()
	fmt.Println("idremesa", idRemesa)
	err = DB.Select(&remxest, `SELECT  DISTINCT idremesa, numrem, estado,
		count(folio) as cant 
	    FROM  tarjetas
		WHERE idremesa =? group by idremesa, numrem, estado`, idRemesa)	
	return
}


//TRAER TODO
func RemxPlaza() (remxplaza []Remxplaza, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&remxplaza, `SELECT id, 
			COALESCE(numrem,'') as numrem, 
			COALESCE(estado,'') as estado,
			COALESCE(cant,'0') as cant,
			COALESCE(guiamens,'') as guiamens, 
			COALESCE(mensajeria,'') as mensajeria,
			COALESCE(fecha,'') as fecha, 
			COALESCE(hora,'') as hora
		FROM remxplaza`)
	return
}

// Insertar un Renglon
func AddRemxPlaza(remxest RemxEst) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO remxplaza (idremesa, numrem, estado, cant)
		VALUES( :idremesa, :numrem, :estado, :cant )`, &remxest)
	return
}

//PARAMETROS: idremesa
func BusIdRemxPlaza(idremesa int) (id Remxplaza, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&id, `SELECT id FROM remxplaza 
		WHERE idremesa=?`, idremesa)
	return
}



func CargarRemxPlaza(numrem string) (remxplaza []Remxplaza, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&remxplaza, `SELECT id, 
			COALESCE(idremesa,'0') as idremesa,
			COALESCE(numrem,'') as numrem, 
			COALESCE(estado,'') as estado,
			COALESCE(cant,'0') as cant,
			COALESCE(guiamens,'') as guiamens, 
			COALESCE(mensajeria,'') as mensajeria,
			COALESCE(fecha,'') as fecha, 
			COALESCE(hora,'') as hora,
			COALESCE(idplaza,'0') as idplaza
		  FROM remxplaza
		  WHERE numrem=?`, numrem)
	return
}


func AutoAsignarEstado(remxplaza Remxplaza) (err error) {
	var DB = modeldb.GetDb()

	_, err = DB.NamedExec(`UPDATE tarjetas
		SET  idplaza =: idplaza 
		WHERE idremesa=255 `, remxplaza)
		fmt.Println("ideplaza = 16")
	return
}



