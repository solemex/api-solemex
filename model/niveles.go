package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
	//"github.com/jmoiron/sqlx"
)

type Niveles struct {
	Id      string `db:"id"     	 	 json:"id"`
	Nivel   string `db:"nivel"     	 	 json:"nivel"`
	Descrip string `db:"descrip"    	 json:"descrip"`
	Acceso  string `db:"acceso"	  		 json:"acceso"`
}

func ListNiveles() (niveles []Niveles, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&niveles, `SELECT id, nivel, descrip, acceso FROM niveles`)
	return
}

func GetNiveles(id string) (niveles Niveles, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&niveles, `SELECT id, nivel, descrip, acceso FROM niveles
		WHERE id=?`, id)
	return
}

func InsertNiveles(niveles Niveles) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO niveles ( nivel, descrip, acceso)
		VALUES(:nivel, :descrip, :acceso )`, &niveles)
	return
}

func UpdateNiveles(niveles Niveles) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE niveles
		SET nivel=:nivel, descrip=:descrip, acceso=:acceso
		WHERE id=:id`, niveles)
	return
}

func DeleteNiveles(id string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM niveles
		WHERE id=?`, id)
	return
}
