package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type BusValida struct {
	NumRem          string `db:"numrem"   json:"numrem"`
	Idremesa		int `db:"idremesa"     		json:"idremesa"`
	Idplaza			int `db:"idplaza"     		json:"idplaza"`
	Idmensajeria	int `db:"idmensajeria"  json:"idmensajeria"`
	Estado			string `db:"estado"     	json:"estado"`
}

type Numcli2 struct {
	Numcli 	string `db:"numcli"     json:"numcli"`
	Id      int    `db:"id"     	json:"id"`
	Estatus string `db:"estatus"   	json:"estatus"`
}

type Correctas struct {
	Id  				  int    `db:"id"     	    json:"id"`
	NumRem        string `db:"numrem"    		json:"numrem"`
	Idremesa		  int    `db:"idremesa"     json:"idremesa"`
	Idplaza			  int    `db:"idplaza"     	json:"idplaza"`
	Idmensajeria	int    `db:"idmensajeria" json:"idmensajeria"`
	Estado			  string `db:"estado"    		json:"estado"`	
	Numguia 		  string `db:"numguia"    	json:"numguia"`
	Fecha_captura string `db:"fecha_captura" json:"fecha_captura"`	
}

type Sobrantes struct {
	Numcli  		  string `db:"numcli"    		json:"numcli"`
	Id  				  int    `db:"id"     	    json:"id"`
	NumRem        string `db:"numrem"    		json:"numrem"`
	Idremesa		  int    `db:"idremesa"     json:"idremesa"`
	Idplaza			  int    `db:"idplaza"     	json:"idplaza"`
	Idmensajeria	int    `db:"idmensajeria" json:"idmensajeria"`
	Estado			  string `db:"estado"    		json:"estado"`	
	Numguia 		  string `db:"numguia"    	json:"numguia"`
	Fecha_captura string `db:"fecha_captura" json:"fecha_captura"`
	Tipo          string `db:"tipo"         json:"tipo"`
}

type Faltantes struct {
	Numcli  		  string `db:"numcli"    		json:"numcli"`
	Id  				  int    `db:"id"     	    json:"id"`
	NumRem        string `db:"numrem"    		json:"numrem"`
	Idremesa		  int    `db:"idremesa"     json:"idremesa"`
	Idplaza			  int    `db:"idplaza"     	json:"idplaza"`
	Idmensajeria	int    `db:"idmensajeria" json:"idmensajeria"`
	Estado			  string `db:"estado"    		json:"estado"`	
	Numguia 		  string `db:"numguia"    	json:"numguia"`
	Fecha_captura string `db:"fecha_captura" json:"fecha_captura"`
	Tipo          string `db:"tipo"         json:"tipo"`	
	Idtarjeta 	  int    `db:"idtarjeta"    json:"idtarjeta"`
}

// ADD SOBRANTES
func AddSobrante(sobrantes Sobrantes) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO diferencias 
			(numcli, idremesa, numrem, idplaza, idmensajeria, estado, tipo)
		VALUES(:numcli, :idremesa, :numrem, :idplaza, :idmensajeria, :estado, "S")`, &sobrantes)
	return
}



// Insertar un Renglon
func AddFaltante(faltante Faltantes) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO diferencias 
		(numcli, numrem, tipo, idplaza, idremesa, idmensajeria, idtarjeta)
		VALUES( :numcli, :numrem, :tipo, :idplaza, :idremesa, :idmensajeria,
		 :idtarjeta)`, &faltante)
	return
}


// UPDATE TARJETAS 
func UpdateCorrectas(correctas Correctas) (err error) {
	var DB = modeldb.GetDb()

	_, err = DB.NamedExec(`UPDATE tarjetas
		SET numguia=:numguia, 
			  estatus= 1,
			  nomestatus="Pendiente",
		    idplaza=:idplaza,
		    Idmensajeria=:idmensajeria,
		    fecha_captura=:fecha_captura
		WHERE id=:id`, correctas)
	fmt.Println(correctas)
	return
}


//PARAMETROS: numguia, codigo
func BusValidaRemxPlaza(idremesa int, estado string) (numclia []Numcli2, err error) {
	var DB = modeldb.GetDb()
	fmt.Println(idremesa, estado)
	 err = DB.Select(&numclia, `SELECT id, numcli, estatus FROM tarjetas
 	WHERE idremesa=? and estado = ?`, idremesa, estado)
	 return
}

