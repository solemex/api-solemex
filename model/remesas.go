package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type IdRem struct {
	ID         string `db:"id"     	 	 json:"id"`
	Numrem     string `db:"numrem"    	 	 json:"numrem"`
	Idcliente  int    `db:"idcliente"   	 json:"idcliente"`
	Idservicio int    `db:"idservicio"      json:"idservicio"`
}

type ConRemesa struct {
	ID        string `db:"id"     	 	 json:"id"`
	Numrem    string `db:"numrem"    	 	 json:"numrem"`
	Numremcli string `db:"numremcli"	  	 json:"numremcli"`
	Fingreso  string `db:"fingreso"     	 json:"fingreso"` //DATE
	Na        int    `db:"na"  	     	 json:"na"`
	Asig      int    `db:"asig"		 	 json:"asig"`
	Di        int    `db:"di"     	 	 json:"di"`
	Dr        int    `db:"dr"     		 json:"dr"`
	En        int    `db:"en"   		 	 json:"en"`
	Tot       int    `db:"tot"			 json:"tot"`
}

type Remesas struct {
	ID          string `db:"id"     	 	 json:"id"`
	Numrem      string `db:"numrem"    	 	 json:"numrem"`
	Numremcli   string `db:"numremcli"	  	 json:"numremcli"`
	Fingreso    string `db:"fingreso"     	 json:"fingreso"` //DATE
	Fvenc       string `db:"fvenc"      	 json:"fvenc"`      //DATE
	Na          int    `db:"na"  	     	 json:"na"`
	Asig        int    `db:"asig"		 	 json:"asig"`
	Di          int    `db:"di"     	 	 json:"di"`
	Dr          int    `db:"dr"     		 json:"dr"`
	En          int    `db:"en"   		 	 json:"en"`
	Tot         int    `db:"tot"			 json:"tot"`
	Ro          int    `db:"ro"      	     json:"ro"`
	Ex          int    `db:"ex"  	     	 json:"ex"`
	Status      int    `db:"status"		 	 json:"status"`
	Fcierre     string `db:"fcierre"     	 json:"fcierre"` //DATE
	Hora_Cierre string `db:"hora_cierre"     json:"hora_cierre"`
	Fenvio      string `db:"fenvio"   		 json:"fenvio"`       //DATE
	Fvenc_real  string `db:"fvenc_real"	 	 json:"fvenc_real"` //DATE
	Remesa      string `db:"remesa"		 	 json:"remesa"`
	Tiporem     int    `db:"tiporem"     	 json:"tiporem"`
	Numuser     int    `db:"numuser"     	 json:"numuser"`
	Idcliente   int    `db:"idcliente"   	 json:"idcliente"`
	Idservicio  int    `db:"idservicio"      json:"idservicio"`
	Nomcli      string `db:"nomcli"   	 	 json:"nomcli"`
	Nomserv     string `db:"nomserv"   	 	 json:"nomserv"`
	Pendientes     int    `json:"pendientes,omitempty"`
	Concat2     string
	Tarjetas    []Tarjetas
}

type Tarjetas struct {
	Idremesa    int    `db:"idremesa"		json:"idremesa"`
	Folio       string `db:"folio"		json:"folio"`
	Remfol      string `db:"remfol"		json:"remfol"`
	Numrem      string `db:"numrem"		json:"numrem"`
	Nomcli      string `db:"nomcli"		json:"nomcli"`
	Numcli      string `db:"numcli"		json:"numcli"`
	Colonia     string `db:"colonia"		json:"colonia"`
	Ciudad      string `db:"ciudad"		json:"ciudad"`
	Estado      string `db:"estado"		json:"estado"`
	Tel1        string `db:"tel1"			json:"tel1"`
	Tel2        string `db:"tel2"			json:"tel2"`
	Tel3        string `db:"tel3"			json:"tel3"`
	Calle       string `db:"calle"	    json:"calle"`
	Cp          string `db:"cp"	        json:"cp"`
	Codigo      string `db:"codigo"	    json:"codigo"`
	Tipotarjeta string `db:"tipotarjeta" 	json:"tipotarjeta"`
	Lugarenvio  string `db:"lugarenvio"	    json:"lugarenvio"`
	Sucursal    string `db:"sucursal"	    json:"sucursal"`
	Email 		string `db:"email"	    json:"email"`
	Numext      string  `db:"numext"	    json:"numext"`
	Tipoent	    string `db:"tipoent"	    json:"tipoent"`
	Idsucbr 	string  `db:"idsucbr"	    json:"idsucbr"`
}

//Estructura para Consulta de Remesas x Status
type StatRem struct {
	Idcliente int   `db:"idcliente"		json:"idcliente"`
	Numrem   string `db:"numrem"		json:"numrem"`
	Fingreso string `db:"fingreso"     	 json:"fingreso"` //DATE
	Fvenc    string `db:"fvenc"      	 json:"fvenc"`      //DATE
	Asig     int    `db:"asig"		 	 json:"asig"`
	Na       int    `db:"na"  	     	 json:"na"`
	Di       int    `db:"di"     	 	 json:"di"`
	En       int    `db:"en"   		 	 json:"en"`
	Ex       int    `db:"ex"   		 	 json:"ex"`
	Tot      int    `db:"tot"			 json:"tot"`
	Pendientes     int    `json:"pendientes,omitempty"`
	Status   int    `db:"status"		 json:"status"`
}


func ListRemesas() (remesas []Remesas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&remesas, `SELECT remesas.id, numrem, 
	COALESCE(numremcli, '') as numremcli, fingreso, fvenc, na, asig, di, dr, en, 
	COALESCE(tot, 0) as tot,  remesas.status,  COALESCE(fcierre,'') as fcierre, 
	COALESCE(hora_cierre,'') as hora_cierre, COALESCE(fenvio,'') as fenvio,
	COALESCE(fvenc_real,'') as fvenc_real, numuser, remesas.idcliente, remesas.idservicio, clientes.nomcli, servicio.nomserv 
	FROM remesas LEFT JOIN clientes on remesas.idcliente = clientes.id
				 LEFT JOIN servicio on remesas.idservicio = servicio.id
	WHERE remesas.status = 1 ORDER by id desc`)
	return
}

func GetRemesas(id string) (remesas Remesas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&remesas, `SELECT id, numrem, numremcli, fingreso, fvenc,
			na, asig, di, dr, en, tot, ro, ex, status, fcierre, hora_cierre,
			fenvio, fvenc_real, remesa, tiporem, numuser, idcliente, idservicio FROM remesas
		WHERE id=?`, id)
	return
}

func InsertRemesas(remesas Remesas) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO remesas ( numrem, fingreso, fvenc, fenvio,
			status, na, asig, di, dr, en, tot, numuser, idcliente, idservicio )
		VALUES( :numrem, :fingreso, :fvenc, :fenvio, 0, :na, :asig, :di, :dr, :en, :tot,
                :numuser, :idcliente, :idservicio )`, &remesas)
	return
}

func InsertTarjetas(tarjetas Tarjetas, idrem []IdRem, idfolio string) (err error) {
	var DB = modeldb.GetDb()
	fmt.Println("Tarjetas nombre ", tarjetas.Nomcli, tarjetas.Numrem)
	// or, you can use MustExec, which panics on error
	addTarjetas := `INSERT INTO tarjetas (idremesa, numrem, numcli,
		    folio, nomcli, calle, colonia, ciudad,
			estado, tel1, tel2, tel3, cp, idcliente, idservicio, codigo, 
			tipotarjeta, sucursal, email, numext, tipoent, idsucbr) 
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`
	DB.MustExec(addTarjetas, idrem[0].ID, idrem[0].Numrem, tarjetas.Numcli,
	     idfolio, tarjetas.Nomcli, tarjetas.Calle, tarjetas.Colonia,
	      tarjetas.Ciudad, tarjetas.Estado, tarjetas.Tel1, tarjetas.Tel2, 
	      tarjetas.Tel3, tarjetas.Cp, idrem[0].Idcliente, idrem[0].Idservicio,
	      tarjetas.Codigo, tarjetas.Tipotarjeta, tarjetas.Sucursal, tarjetas.Email,
	      tarjetas.Numext, tarjetas.Tipoent, tarjetas.Idsucbr)
	return
}

func GetidRem() (IdRem []IdRem, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&IdRem, `SELECT id, numrem , idcliente, idservicio FROM remesas ORDER BY id desc limit 1`)
	return
}

//Consulta de Remesas por Status
func EstatusRem(status int, idcliente int) (remesas []Remesas, err error) {
	var DB = modeldb.GetDb()

	err = DB.Select(&remesas, `SELECT remesas.id, numrem, 
	COALESCE(numremcli, '') as numremcli,
	fingreso, fvenc, na, asig, di, dr, en, ex,
	(remesas.tot - remesas.en- remesas.dr  ) as pendientes,  
	COALESCE(tot, 0) as tot, 
	remesas.status, 
	COALESCE(fcierre,'') as fcierre, 
	COALESCE(hora_cierre,'') as hora_cierre, 
	COALESCE(fenvio,'') as fenvio,
	COALESCE(fvenc_real,'') as fvenc_real,
	numuser, remesas.idcliente, remesas.idservicio, clientes.nomcli, servicio.nomserv 
	FROM remesas LEFT JOIN clientes on remesas.idcliente = clientes.id
				 LEFT JOIN servicio on remesas.idservicio = servicio.id
	WHERE remesas.status = ? and 
		  remesas.idcliente = ?
	ORDER by id desc`, status, idcliente )
	return
}


func RemxValidar() (remesas []Remesas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&remesas, `SELECT remesas.id, numrem, 
	COALESCE(numremcli, '') as numremcli,
	fingreso, fvenc, na, asig, di, dr, en, 
	COALESCE(tot, 0) as tot, 
	remesas.status, 
	COALESCE(fcierre,'') as fcierre, 
	COALESCE(hora_cierre,'') as hora_cierre, 
	COALESCE(fenvio,'') as fenvio,
	COALESCE(fvenc_real,'') as fvenc_real,
	numuser, remesas.idcliente, remesas.idservicio
	FROM remesas 
	WHERE remesas.status = 0 
	ORDER by id desc`)
	return
}




type Terminar struct {
   	Id   	 int    `db:"id"		json:"id"`
 	Status   int    `db:"status"	json:"status"`
}

func ProcesaRem (terminar Terminar) (err error) {
	var DB = modeldb.GetDb()
	fmt.Println("terminar model", terminar)

	_, err = DB.NamedExec(`UPDATE remesas 
		SET status=:status
		WHERE id=:id`, terminar)
	return
}


type Cerrar struct {
   	Id   	 int    `db:"id"		json:"id"`
 	Status   int    `db:"status"	json:"status"`
 	Fcierre string  `db:"fcierre"	json:"fcierre"`
 	Hora_cierre string   `db:"hora_cierre"	json:"hora_cierre"`
}

func CerrarRem (cerrar Cerrar) (err error) {
	var DB = modeldb.GetDb()
	fmt.Println("terminar model", cerrar)

	_, err = DB.NamedExec(`UPDATE remesas 
		SET status=:status,
			fcierre=:fcierre,
			hora_cierre=:hora_cierre
		WHERE id=:id`, cerrar)
	return
}


//Consulta de Remesas por Status
func EstatusRemAlta(status int) (remesas []Remesas, err error) {
	var DB = modeldb.GetDb()

	err = DB.Select(&remesas, `SELECT remesas.id, numrem, 
	COALESCE(numremcli, '') as numremcli,
	fingreso, fvenc, na, asig, di, dr, en, ex,
	COALESCE(tot, 0) as tot, 
	remesas.status, 
	COALESCE(fcierre,'') as fcierre, 
	COALESCE(hora_cierre,'') as hora_cierre, 
	COALESCE(fenvio,'') as fenvio,
	COALESCE(fvenc_real,'') as fvenc_real,
	numuser, remesas.idcliente, remesas.idservicio, clientes.nomcli, servicio.nomserv 
	FROM remesas LEFT JOIN clientes on remesas.idcliente = clientes.id
				 LEFT JOIN servicio on remesas.idservicio = servicio.id
	WHERE remesas.status = ? 
	ORDER by id desc`, status )
	return
}

