package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type Cliente struct {
	ID       string `db:"id"    	  json:"id"`
	Nomcli   string `db:"nomcli"   json:"nomcli"`
	Rfc      string `db:"rfc"	  json:"rfc"`
	Calle    string `db:"calle"    json:"calle"`
	Numext   string `db:"numext"    json:"numext"`
	Colonia  string `db:"colonia"    json:"colonia"`
	Ciudad   string `db:"ciudad"   json:"ciudad"`
	Cp       string `db:"cp"   	  json:"cp"`
	Estado   string `db:"estado"   json:"estado"`
	Telefono string `db:"telefono"   json:"telefono"`
	Email    string `db:"email"   json:"email"`
}

func ListClientes() (cliente []Cliente, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&cliente, `SELECT id, nomcli, rfc, calle, numext, colonia, 
		ciudad, cp, estado, telefono, email FROM clientes`)
	return
}

func GetCliente(id string) (cliente Cliente, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&cliente, `SELECT id, nomcli, rfc, calle, numext,
			 colonia, ciudad, cp, estado, telefono, email FROM clientes
		WHERE id=?`, id)
	return
}

func InsertCliente(cliente Cliente) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO clientes (nomcli, rfc, calle, numext,
				colonia, ciudad, cp, estado, telefono, email)
		VALUES(:nomcli, :rfc, :calle, :numext, :colonia, :ciudad, :cp,
			   :estado, :telefono, :email)`, &cliente)
	return
}

func UpdateCliente(cliente Cliente) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE clientes
		SET nomcli=:nomcli, rfc=:rfc, calle=:calle, numext=:numext, colonia=:colonia,
			ciudad=:ciudad, cp=:cp, estado=:estado, telefono=:telefono, email=:email
		WHERE id=:id`, cliente)
	return
}

func DeleteCliente(id string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM clientes
		WHERE id=?`, id)
	return
}
