package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type Sucursales struct {
	Id         string `db:"id"     	 	 json:"id"`
	Numsuc     string `db:"numsuc"    	 json:"numsuc"`
	Nomsuc     string `db:"nomsuc"	  	 json:"nomsuc"`
	Nomcont    string `db:"nomcont"      json:"nomcont"`
	Direccion  string `db:"direccion"    json:"direccion"`
	CP         string `db:"cp"   	     json:"cp"`
	Colonia    string `db:"colonia"	 	 json:"colonia"`
	Ciudad     string `db:"ciudad"     	 json:"ciudad"`
	Estado     string `db:"estado"       json:"estado"`
	Pais       string `db:"pais" 		 json:"pais"`
	Telefono   string `db:"telefono"     json:"telefono"`
	Status     int    `db:"status"     	 json:"status"`
	Idcliente  int    `db:"idcliente"    json:"idcliente"`
	Idservicio int    `db:"idservicio"	 json:"idservicio"`
}

type NewSucursal struct{
	Id         string `db:"id"     	 	 json:"id"`
	Numsuc     string `db:"numsuc"    	 json:"numsuc"`
	Nomsuc     string `db:"nomsuc"	  	 json:"nomsuc"`
	Direccion  string `db:"direccion"    json:"direccion"`
	CP         string `db:"cp"   	     json:"cp"`
	Colonia    string `db:"colonia"	 	 json:"colonia"`
	Ciudad     string `db:"ciudad"     	 json:"ciudad"`
	Estado     string `db:"estado"       json:"estado"`
	Telefono   string `db:"telefono"     json:"telefono"`
	Idcliente  int    `db:"idcliente"    json:"idcliente"`
}

type EditSucursal struct{
	Id         string `db:"id"     	 	 json:"id"`
	Numsuc     string `db:"numsuc"    	 json:"numsuc"`
	Nomsuc     string `db:"nomsuc"	  	 json:"nomsuc"`
	Direccion  string `db:"direccion"    json:"direccion"`
	CP         string `db:"cp"   	     json:"cp"`
	Colonia    string `db:"colonia"	 	 json:"colonia"`
	Ciudad     string `db:"ciudad"     	 json:"ciudad"`
	Estado     string `db:"estado"       json:"estado"`
	Telefono   string `db:"telefono"     json:"telefono"`
	Idcliente  int    `db:"idcliente"    json:"idcliente"`
	Nomcli     string `db:"nomcli"       json:"nomcli"`
}

type Catsuc	struct {
	Id     int    `db:"id"    	json:"id"`
	Numsuc string `db:"numsuc"  json:"numsuc"`
	Nomsuc string `db:"nomsuc"  json:"nomsuc"`
	Nomcli string `db:"nomcli"  json:"nomcli"`
	Ciudad string `db:"ciudad"  json:"ciudad"`
	Estado string `db:"estado"  json:"estado"`
}

type SucxCli	struct {
	Id     int    `db:"id"    	json:"id"`
	Numsuc string `db:"numsuc"  json:"numsuc"`
	Nomsuc string `db:"nomsuc"  json:"nomsuc"`
	Nomcli string `db:"nomcli"  json:"nomcli"`
	Ciudad string `db:"ciudad"  json:"ciudad"`
	Estado string `db:"estado"  json:"estado"`
	Idcliente  int    `db:"idcliente"    json:"idcliente"`

}


// Listado de sucursales x cliente
func ListSucursalesxCli(id int) (sucxcli []SucxCli, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&sucxcli, `SELECT sucursales.id ,sucursales.numsuc, sucursales.nomsuc, sucursales.ciudad, sucursales.estado, sucursales.idcliente, clientes.nomcli
     FROM sucursales LEFT JOIN clientes on sucursales.idcliente = clientes.id WHERE sucursales.idcliente = ?`, id)
	return
}

func ListCatSuc() (catsuc []Catsuc, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&catsuc, `SELECT sucursales.id ,sucursales.numsuc, sucursales.nomsuc, sucursales.ciudad, sucursales.estado, clientes.nomcli 
     FROM sucursales LEFT JOIN clientes on sucursales.idcliente = clientes.id `)
	return
}



func ListSucursales() (sucursales []Sucursales, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&sucursales, `SELECT id, numsuc, COALESCE(nomsuc,'') as nomsuc, COALESCE(nomcont,'') as nomcont,
			 COALESCE(direccion,'') as direccion,  COALESCE(cp,'') as cp, COALESCE(colonia,'') as colonia, 
			 COALESCE(ciudad,'') as ciudad, COALESCE(estado,'') as estado, COALESCE(pais,'') as pais,
			 COALESCE(telefono,'') as telefono, status, idcliente, idservicio
		     FROM sucursales`)
	return
}

func SucxNumsuc(id int, numsuc string) (sucursales EditSucursal, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&sucursales, `SELECT s.id, s.numsuc, COALESCE(s.nomsuc,'') as nomsuc, COALESCE(s.direccion,'') as direccion,  
		     COALESCE(s.cp,'') as cp, COALESCE(s.colonia,'') as colonia, 
			 COALESCE(s.ciudad,'') as ciudad, COALESCE(s.estado,'') as estado,
			 COALESCE(s.telefono,'') as telefono, s.idcliente, clientes.nomcli
		    FROM sucursales s LEFT JOIN clientes on s.idcliente = clientes.id 
		WHERE s.numsuc= ? AND s.idcliente = ?`, numsuc, id)
	return
}

func GetSucursales(id string) (sucursales EditSucursal, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&sucursales, `SELECT s.id, s.numsuc, COALESCE(s.nomsuc,'') as nomsuc, COALESCE(s.direccion,'') as direccion,  
		     COALESCE(s.cp,'') as cp, COALESCE(s.colonia,'') as colonia, 
			 COALESCE(s.ciudad,'') as ciudad, COALESCE(s.estado,'') as estado,
			 COALESCE(s.telefono,'') as telefono, s.idcliente, clientes.nomcli
		    FROM sucursales s LEFT JOIN clientes on s.idcliente = clientes.id 
		WHERE s.id= ? `, id)
	return
}

func InsertSucursales(sucursales NewSucursal) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO sucursales ( numsuc, nomsuc, direccion, 
			cp, colonia, ciudad, estado, telefono, idcliente)
		VALUES(:numsuc, :nomsuc, :direccion, :cp, :colonia, :ciudad, 
			   :estado, :telefono, :idcliente )`, &sucursales)
	return
}


func UpdateSucursales(sucursales Sucursales) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE sucursales
		SET numsuc=:numsuc, nomsuc=:nomsuc, nomcont=:nomcont, direccion=:direccion,
			cp=:cp, colonia=:colonia, ciudad=:ciudad, estado=:estado, pais=:pais,
		    telefono=:telefono,  status=:status, idcliente=:idcliente, idservicio=:idservicio
		WHERE id=:id`, sucursales)
	return
}

func DeleteSucursales(id string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM sucursales
		WHERE id=?`, id)
	return
}
