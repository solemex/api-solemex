package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type BusTarj struct {
	Id         string `db:"id"  json:"id"`
	Numsuc     string `db:"numsuc"  json:"numsuc"`
	Nomsuc     string `db:"nomsuc"  json:"nomsuc"`
	Idsucbr    string `db:"idsucbr" json:"idsucbr"`
	Numrem     string `db:"numrem" json:"numrem"`
	Numcli     string `db:"numcli" json:"numcli"`
}

type PutIdSuc struct {
	Id         string `db:"id"      json:"id"`
	Idsucbr    string `db:"idsucbr" json:"idsucbr"`
}

func TarjSinSuc(numrem string , idsucbr string) (bustarj []BusTarj, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&bustarj, `SELECT tarjetas.numrem, tarjetas.idsucbr, tarjetas.id, 
			tarjetas.numcli, sucursales.nomsuc
		 FROM tarjetas  LEFT JOIN sucursales ON tarjetas.idsucbr = sucursales.numsuc
		WHERE numrem=? AND idsucbr=?`, numrem, idsucbr)
	return
}

func PutIdSucBr(putidsuc PutIdSuc) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE tarjetas
		SET idsucbr=:idsucbr
		WHERE id=:id `, putidsuc)
	return
}

