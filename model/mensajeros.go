//Modelo Mensajeros

package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type Mensajeros struct {
	ID            string `db:"id"     	 	 json:"id"`
	Nombre        string `db:"nombre"    	 json:"nombre"`
	Nomuser       string `db:"nomuser"	  	 json:"nomuser"`
	Password      string `db:"password"      json:"password"`
	Estatus       int    `db:"estatus"       json:"estatus"`
	Idmensajeria  int    `db:"idmensajeria"  json:"idmensajeria"`
	Nommensajeria string `db:"nommensajeria" json:"nommensajeria"`
	Add_Numuser   int    `db:"add_numuser"   json:"add_numuser"`
	Edit_Numuser  int    `db:"edit_numuser"	 json:"edit_numuser"`
	Ife           string `db:"ife"   		 json:"ife"`
	Fotoife1      string `db:"fotoife1"		 json:"fotoife1"`
	Fotoife2      string `db:"fotoife2"	 	 json:"fotoife2"`
}

// Consulta de mensajeros por mensajeria
func ListMensajerosxMia(id int) (mensajeros []Mensajeros, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&mensajeros, `SELECT coalesce(mensajerias.nommensajeria, '') as nommensajeria,
	mensajeros.id, mensajeros.nombre, nomuser, password, 
	mensajeros.estatus, add_numuser, idmensajeria, edit_numuser, coalesce(mensajerias.id, 0) as idmensajeria
	FROM mensajeros LEFT JOIN 	mensajerias on mensajeros.idmensajeria = mensajerias.id WHERE mensajeros.idmensajeria=?`,id)
	return
}


func ListMensajeros() (mensajeros []Mensajeros, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&mensajeros, `SELECT coalesce(mensajerias.nommensajeria, '') as nommensajeria,
	mensajeros.id, mensajeros.nombre, nomuser, password, 
	mensajeros.estatus, add_numuser, idmensajeria, edit_numuser, coalesce(mensajerias.id, 0) as idmensajeria
	FROM mensajeros LEFT JOIN 	mensajerias on mensajeros.idmensajeria = mensajerias.id `)
	return
}

func GetMensajeros(id string) (mensajeros Mensajeros, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&mensajeros, `SELECT  coalesce(mensajerias.nommensajeria, '') as nommensajeria,mensajeros.id, mensajeros.nombre, nomuser, password, 
	mensajeros.estatus, add_numuser, idmensajeria, edit_numuser, coalesce(mensajerias.id, 0) as idmensajeria
	FROM mensajeros LEFT JOIN 	mensajerias on mensajeros.idmensajeria = mensajerias.id 
	WHERE mensajeros.id=?`, id)
	return
}

func InsertMensajeros(mensajeros Mensajeros) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO mensajeros (  nombre, nomuser, password, 
			 estatus, idmensajeria, add_numuser, edit_numuser, ife, fotoife1, fotoife2)
		VALUES(:nombre, :nomuser, :password, :estatus, :idmensajeria, :add_numuser, :edit_numuser,
			   :ife, :fotoife1, :fotoife2)`, &mensajeros)
	return
}

func UpdateMensajeros(mensajeros Mensajeros) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE mensajeros
		SET nombre=:nombre, nomuser=:nomuser, password=:password, estatus=:estatus,
			idmensajeria=:idmensajeria, add_numuser=:add_numuser, edit_numuser=:edit_numuser, ife=:ife,
			fotoife1=:fotoife1, fotoife2=:fotoife2
		WHERE id=:id`, mensajeros)
	return
}

func DeleteMensajeros(id string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM mensajeros
		WHERE id=?`, id)
	return
}

func ValidaInfo(nomuser string) (mensajeros Mensajeros, err error) {
	var DB = modeldb.GetDb()
	fmt.Println(nomuser)
	err = DB.Select(&mensajeros, `SELECT  mensajeros.id, mensajeros.nombre,
	    mensajeros.nomuser, mensajeros.estatus
		FROM mensajeros 
		WHERE mensajeros.nomuser=?`, nomuser)
	return
}

//PARAMETROS: nomuser
func BusIdValuser(nomuser string) (id Mensajeros, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&id, `SELECT id FROM mensajeros 
		WHERE mensajeros.nomuser=?`, nomuser)
	//fmt.Println("BuscarIdKey =", err)
	return
}


// func LoginAppMovil22(nomuser string, password string) (loginapp []LoginApp, err error) {
// 	var DB = modeldb.GetDb()
// 	fmt.Println("nomuser, password ", nomuser, password)

// 	err = DB.Select(&loginapp, `SELECT  mensajeros.id, mensajeros.nombre,
// 	    mensajeros.nomuser, mensajeros.estatus
// 		FROM mensajeros 
// 		WHERE mensajeros.estatus = 2 and 
// 		mensajeros.nomuser = ? AND mensajeros.password = ? `, nomuser, password)
// 	return
// }