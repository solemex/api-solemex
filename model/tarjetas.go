package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type CatTarjetas struct {
	Id               int    `db:"id"     	   	     json:"id"`
	Numguia          string `db:"numguia"   	   	 json:"numguia"`
	Idremesa         string `db:"idremesa"      	 json:"idremesa"`
	Numrem           string `db:"numrem"   	   	 	 json:"numrem"`
	Remesam          int    `db:"remesam"   	   	 json:"remesam"`
	Folio            string `db:"folio"   	   	 	 json:"folio"`
	Remfol           string `db:"remfol"   	   	 	 json:"remfol"`
	Fvenc            string `db:"fvenc"   	   	 	 json:"fvenc"` //DATE
	Cant             int    `db:"cant"   	   	 	 json:"cant"`
	Numcli           string `db:"numcli"  	   	 	 json:"numcli"`
	Nomcli           string `db:"nomcli"  	   	 	 json:"nomcli"`
	Numconsig        string `db:"numconsig"     	 json:"numconsig"`
	Nomconsig        string `db:"nomconsig"     	 json:"nomconsig"`
	Fecha_captura    string `db:"fecha_captura"   	 json:"fecha_captura"` //DATE
	Contenido_envio  string `db:"contenido_envio" 	 json:"contenido_envio"`
	Folio_cliente    string `db:"folio_cliente"   	 json:"folio_cliente"`
	Latitude         string `db:"latitude"   	 	 json:"latitude"`
	Longitude        string `db:"longitude"   	 	 json:"longitude"`
	Nomrecibe        string `db:"nomrecibe"   	 	 json:"nomrecibe"`
	Parentesco       string `db:"parentesco"   	 	 json:"parentesco"`
	Tipoid           string `db:"tipoid"   	 	     json:"tipoid"`
	Direccionentrega string `db:"direccionEntrega"   json:"direccionEntrega"` //Text
	Numid            string `db:"numid"   	 		 json:"numid"`
	Fecha_entrega    string `db:"fecha_entrega"   	 json:"fecha_entrega"` //DATE
	Estatus          int    `db:"estatus" 			 json:"estatus"`
	Nomestatus       string `db:"nomestatus"   	     json:"nomestatus"`
	Hora_entrega     string `db:"hora_entrega"   	 json:"hora_entrega"`
	Concat2          string `db:"concat2"   		 json:"concat2"`
	Numuser          int    `db:"numuser"   	 	 json:"numuser"`
	Edit_numuser     int    `db:"edit_numuser"   	 json:"edit_numuser"`
	Mensajero        int    `db:"mensajero"     	 json:"mensajero"`
	Idmensaeria      int    `db:"idmensajeria"     	 json:"idmensajeria"`
	Fecha_asignacion string `db:"fecha_asignacion"   json:"fecha_asignacion"` //DATE
	Hora_asignacion  string `db:"hora_asignacion"    json:"hora_asignacion"`
	User_asignacion  int    `db:"user_asignacion" 	 json:"user_asignacion"`
	Num_intentos     int    `db:"num_intentos"   	 json:"num_intentos"`
	Tipoconsig       string `db:"tipoconsig"   	 	 json:"tipoconsig"`
	Iddom            int    `db:"iddom"   	  	     json:"iddom"`
	Codigo           string `db:"codigo"   	 		 json:"codigo"`
	Tipotarjeta      string `db:"tipotarjeta"   	 json:"tipotarjeta"`
	Lugarenvio       string `db:"lugarenvio"   	 	 json:"lugarenvio"`
	Sucursal         string `db:"sucursal"  	   	 json:"sucursal"`
	Fecha_print      string `db:"fecha_print"  	   	 json:"fecha_print"` //DATE
	Hora_print       string `db:"hora_print"     	 json:"hora_print"`
	Idcliente        int    `db:"idcliente"   	 	 json:"idcliente"`
	Idservicio       int    `db:"idservicio"   	 	 json:"idservicio"`
	Idremsuc         int    `db:"idremsuc"   	 	 json:"idremsuc"`
	Idremdom         int    `db:"idremdom"   	 	 json:"idremdom"`
	Idconsuc         int    `db:"idconsuc"   	 	 json:"idconsuc"`
	Idcondom         int    `db:"idcondom"   	 	 json:"idcondom"`
	Paqueteria       int    `db:"paqueteria"   	 	 json:"paqueteria"`
	Nomrem           string `db:"nomrem"   	 	 	 json:"nomrem"`
	Calle            string `db:"calle"  	   	 	 json:"calle"`
	Numext           string `db:"numext"     	 	 json:"numext"`
	Cp               string `db:"cp"     			 json:"cp"`
	Colonia          string `db:"colonia"   		 json:"colonia"`
	Ciudad           string `db:"ciudad" 			 json:"ciudad"`
	Pais             string `db:"pais"     			 json:"pais"`
	Tel1             string `db:"tel1"   			 json:"tel1"`
	Tel2             string `db:"tel2" 			     json:"tel2"`
	Tel3             string `db:"tel3" 			     json:"tel3"`
	Nummensajeria    string `db:"nummensajeria" 	 json:"nummensajeria"`
	Nommensajeria    string `db:"nommensajeria" 	 json:"nommensajeria"`
	Nombre           string `db:"nombre" 			 json:"nombre"`
	Estado           string  `db:"estado" 			 json:"estado"`
}

type TarjxEstatus struct {
	Id            int    `db:"id"     	   	    json:"id"`
	Estatus       int    `db:"estatus" 		  json:"estatus"`
	Fingreso	    string `db:"fingreso"   	  json:"fingreso"`
	Idcliente 	  int    `db:"idcliente"   	  json:"idcliente"`
	Idservicio    string `db:"idservicio"     json:"idservicio"`
	Numguia       string `db:"numguia"   	  json:"numguia"`
	Numcli        string `db:"numcli"  	   	  json:"numcli"`
	Nomcli        string `db:"nomcli"  	   	  json:"nomcli"`
	Numrem        string `db:"numrem"   	  json:"numrem"`
	Codigo        string `db:"codigo"   	  json:"codigo"`
	Nomestatus    string `db:"nomestatus"     json:"nomestatus"`
	Nombre        string `db:"nombre" 		  json:"nombre"`
	Nomrecibe     string `db:"nomrecibe"   	  json:"nomrecibe"`
	Fecha_entrega string `db:"fecha_entrega"  json:"fecha_entrega"` 
}

type Folio struct {
	Id         int    `db:"id"     	   	    json:"id"`
	Mensajero  int    `db:"mensajero"     	json:"mensajero"`
	Numcli     string `db:"numcli"     		json:"numcli"`
	Numguia    string `db:"numguia"     	json:"numguia"`
	Idremesa   string `db:"idremesa"      	json:"idremesa"`
	Idcliente  int    `db:"idcliente"   	json:"idcliente"`
	Idservicio int    `db:"idservicio"   	json:"idservicio"`
}

type Supervision struct {
	Mensajero     int    `db:"mensajero"     json:"mensajero"`
	Estatus       int    `db:"estatus" 		   json:"estatus"`
	Numguia       string `db:"numguia"   	   json:"numguia"`
	Fecha_entrega string `db:"fecha_entrega" json:"fecha_entrega"` 
	Hora_entrega  string `db:"hora_entrega"  json:"hora_entrega"`
	Tipoent       string `db:"tipoent"   	   json:"tipoent"`  
	Latitude      string `db:"latitude"   	 json:"latitude"`
	Longitude     string `db:"longitude"   	 json:"longitude"`

}

// <<<<<<<<<<<< APARTADO PARA FUNCIONES SQL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

// GET SUPERVISION X MENSAJERO 
// *** NOTA : SE TIENE QUE AGREGAR CONDICION POR FECHAS
func GetSuperTarj(mensajero int , fecha_entrega string) (supervision []Supervision, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&supervision, `SELECT  numguia, estatus, mensajero,
		   COALESCE(tipoent, '')      as tipoent,
	     COALESCE(latitude,'')      as latitude     , COALESCE(longitude,'')    as longitude, 
       COALESCE(fecha_entrega,'') as fecha_entrega, COALESCE(hora_entrega,'') as hora_entrega
	FROM tarjetas WHERE mensajero=? AND fecha_entrega =? ORDER BY hora_entrega`, mensajero, fecha_entrega)
	return
}

// GET TARJETAS
func ListCatTarjetas() (tarjetas []TarjxEstatus, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&tarjetas, `SELECT  COALESCE(numguia,'') as numguia, COALESCE(numcli,'') as numcli,
	   COALESCE(nomcli,'') as nomcli, COALESCE(numrem,'') as numrem,  COALESCE(codigo,'') as codigo,
	   nomestatus, COALESCE(mensajeros.nombre,'') as nombre, COALESCE(nomrecibe,'') as nomrecibe,
	   COALESCE(fecha_entrega,'') as fecha_entrega
	FROM tarjetas
	LEFT JOIN mensajeros ON  tarjetas.mensajero = mensajeros.id
    LEFT JOIN mensajerias ON  mensajeros.idmensajeria = mensajerias.id`)
	return
}
// GET TARJETASA By ID
func GetCatTarjetas(id string) (tarjetas CatTarjetas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&tarjetas, `SELECT id, numguia, COALESCE(idremesa,'0')  as idremesa, COALESCE(numrem,'') as numrem, COALESCE(numcli,'') as numcli, 
	   COALESCE(nomcli,'') as nomcli, fecha_captura, COALESCE(latitude,'') as latitude, COALESCE(longitude,'') as longitude, 
       COALESCE(nomrecibe,'') as nomrecibe, COALESCE(parentesco,'') as parentesco, COALESCE(tipoid,'') as tipoid,
       COALESCE(numid,'') as numid, COALESCE(fecha_entrega,'') as fecha_entrega, estatus, nomestatus, COALESCE(hora_entrega,'') as hora_entrega,
       concat2, COALESCE(iddom,'0') as iddom, COALESCE(codigo,'') as codigo, COALESCE(tipotarjeta, '') as tipotarjeta, COALESCE(fecha_print,'') as fecha_print,
       COALESCE(hora_print,'') as hora_print, COALESCE(mensajero,'0') as mensajero, COALESCE(fecha_asignacion,'') as fecha_asignacion, COALESCE(hora_asignacion,'') as hora_asignacion,
       COALESCE(user_asignacion,'0') as user_asignacion, num_intentos, idcliente, idservicio, COALESCE(lugarenvio,'') as lugarenvio,
       COALESCE(sucursal,'') as sucursal, COALESCE(calle,'') as calle, COALESCE(numext,'') as numext, COALESCE(cp,'') as cp, 
       COALESCE(colonia,'') as colonia, COALESCE(ciudad,'') as ciudad, COALESCE(pais,'') as pais,COALESCE(tel1,'') as tel1, COALESCE(tel2,'') as tel2, COALESCE(tel3,'') as tel3
	FROM tarjetas
	   WHERE id=?`, id)
	return
}
// GET TARJETASA By ID
func GetTarxRem(idremesa string) (tarjetas []CatTarjetas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&tarjetas, `SELECT tarjetas.id, 
		COALESCE( numguia,'') AS numguia, 
		COALESCE(idremesa,'0')  as idremesa, 
		COALESCE(numrem,'') as numrem, 
		COALESCE(numcli,'') as numcli, 
	    COALESCE(nomcli,'') as nomcli, 
	    COALESCE(folio,'') as folio,    
	    COALESCE( fecha_captura,'' ) as fecha_captura, 
	    COALESCE(latitude,'') as latitude, 
	    COALESCE(longitude,'') as longitude, 
        COALESCE(nomrecibe,'') as nomrecibe, 
        COALESCE(parentesco,'') as parentesco, 
        COALESCE(tipoid,'') as tipoid,
        COALESCE(numid,'') as numid, 
        COALESCE(fecha_entrega,'') as fecha_entrega, 
        tarjetas.estatus, tarjetas.nomestatus, 
        COALESCE(hora_entrega,'') as hora_entrega,
        COALESCE(iddom,'0') as iddom, 
        COALESCE(codigo,'') as codigo, 
        COALESCE(tipotarjeta, '') as tipotarjeta, 
        COALESCE(fecha_print,'') as fecha_print,
        COALESCE(hora_print,'') as hora_print, 
        COALESCE(mensajerias.nummensajeria,'') as nummensajeria, 
        COALESCE(mensajerias.nommensajeria,'') as nommensajeria, 
        mensajeros.idmensajeria, 
        COALESCE(mensajero,'0') as mensajero, mensajeros.nombre,
        COALESCE(fecha_asignacion,'') as fecha_asignacion, 
        COALESCE(hora_asignacion,'') as hora_asignacion,
        COALESCE(user_asignacion,'0') as user_asignacion, num_intentos,
        COALESCE(idcliente,'0') AS idcliente, 
        COALESCE(idservicio,'0') AS idservicio,
        COALESCE(lugarenvio,'') as lugarenvio,
        COALESCE(sucursal,'') as sucursal, 
        COALESCE(calle,'') as calle, 
        COALESCE(numext,'') as numext, 
        COALESCE(cp,'') as cp, 
        COALESCE(colonia,'') as colonia,
        COALESCE(ciudad,'') as ciudad, 
        COALESCE(tarjetas.estado,'') as estado, 
        COALESCE(pais,'') as pais,
        COALESCE(tel1,'') as tel1,
        COALESCE(tel2,'') as tel2,
        COALESCE(tel3,'') as tel3	   
    FROM tarjetas
    LEFT JOIN mensajeros ON  tarjetas.mensajero = mensajeros.id
    LEFT JOIN mensajerias ON  mensajeros.idmensajeria = mensajerias.id
	   WHERE idremesa=?`, idremesa)
	return
}

//PARAMETROS: numguia
func BuscarFolio(numguia string) (folio []Folio, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&folio, `SELECT idcliente, idservicio 
			FROM tarjetas
			WHERE numguia=?`, numguia)
	return
}

func TarjxStatus(estatus int, idcliente int) (tarjetas []TarjxEstatus, err error) {
	var DB = modeldb.GetDb()
	 

	err = DB.Select(&tarjetas, `SELECT COALESCE(tarjetas.numguia,'') as numguia, 
		remesas.idcliente, remesas.idservicio, 
		remesas.fingreso,		
		COALESCE(tarjetas.numcli,'') as numcli,
	   	COALESCE(tarjetas.nomcli,'') as nomcli,  
	   	COALESCE(tarjetas.numrem,'') as numrem, 
	   	COALESCE(tarjetas.codigo,'') as codigo, 
	   	nomestatus, COALESCE(mensajeros.nombre,'') as nombre,
	   	COALESCE(tarjetas.nomrecibe,'') as nomrecibe, 
	   	COALESCE(tarjetas.fecha_entrega,'') as fecha_entrega, tarjetas.estatus
	FROM tarjetas 
		LEFT JOIN remesas ON tarjetas.idremesa = remesas.id
		LEFT JOIN mensajeros ON  tarjetas.mensajero = mensajeros.id
	    LEFT JOIN mensajerias ON  mensajeros.idmensajeria = mensajerias.id
	WHERE
		tarjetas.estatus=? AND 
		tarjetas.idcliente=? AND
		remesas.fingreso`, estatus, idcliente)
	return
}





