package model

import (
	"gitlab.com/solemex/api-solemex/modeldb"
	_ "github.com/go-sql-driver/mysql"
)

type PreGuias struct {
	ID          string `db:"id"     	 	 json:"id"`
	Idremesa	int 	`db:"idremesa"     	 json:"idremesa"`
	Numrem      string  `db:"numrem"     	 json:"numrem"`
	Numcli      string `db:"numcli"    	 	 json:"numcli"`
	Nomcli      string `db:"nomcli"    	 	 json:"nomcli"`
	Codigo 		string `db:"codigo"    	 	json:"codigo"`
	Tipotarjeta string `db:"tipotarjeta"    json:"tipotarjeta"`
	Lugarenvio 	string `db:"lugarenvio"    	json:"lugarenvio"`
	Sucursal 	string `db:"sucursal"    	json:"sucursal"`
	Iddom 		string `db:"iddom"    	 	json:"iddom"`
	Concat2 	string `db:"concat2"    	 json:"concat2"`
	Estatus 	int   `db:"estatus"    	 	 json:"estatus"`
	
}


// Consulta general de guias. Mayores a 150
func ListPreGuias() (preguias []PreGuias, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&preguias, `SELECT id, idremesa, numcli, nomcli, codigo, tipotarjeta, 
		lugarenvio, sucursal, iddom, concat2, estatus 
		FROM preguias WHERE idremesa > 150`)
	return
}

func GetPreGuias(idremesa string) (preguias []PreGuias, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&preguias, `SELECT id, idremesa, numcli, nomcli, codigo, tipotarjeta, 
		lugarenvio, sucursal, iddom, concat2, estatus 
		FROM preguias WHERE idremesa =?`, idremesa)
	return
}

func GetPreGuiasRem(idremesa string) (guias []GuiasCon, err error) {
	var DB = modeldb.GetDb()
		err = DB.Select(&guias, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, estatus, concat2,
			iddom, idcliente, idservicio, fecha_captura FROM guias WHERE idremesa =?`, idremesa)
	return
}

func InsertPreGuias (preguias PreGuias) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO guias ( numrem, fingreso, fvenc, fenvio,
			status, na, asig, di, dr, en, tot, numuser, idcliente, idservicio )
		VALUES( :numrem, :fingreso, :fvenc, :fenvio, :status, :na, :asig, :di, :dr, :en, :tot,
                :numuser, :idcliente, :idservicio )`, &preguias)
	
	return
}
