package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"

)

type Guiarem struct {
	Id 		  int   `db:"id"   	   	 json:"id"`
	Numguia   string `db:"numguia"   json:"numguia"`
	Idremesa  int    `db:"idremesa"  json:"idremesa"`
	Estatus   int    `db:"estatus"   json:"estatus"`
	Nomestatus string `db:"nomestatus"  json:"nomestatus"`
}

	
// GET TARJETASA By ID
func DevolucionDirecta(numguia string) (guiarem Guiarem, err error) {
	var DB = modeldb.GetDb()
	//fmt.Println("guia", numguia)
	
	// Consultar la idRemesa en Tarjetas	
	err = DB.Get(&guiarem, `SELECT id, 
		coalesce(idremesa,0) as idremesa
		from tarjetas 
		where tarjetas.numguia =? `, numguia)

	//fmt.Println("idremesa", guiarem.Id, guiarem.Idremesa)

	guiarem.Estatus = 4
	guiarem.Nomestatus = "Devueltas"

	fmt.Println("idremesa", guiarem.Id, guiarem.Idremesa, guiarem.Nomestatus)

	//Actualizar Tajeta Devuelta en Forma Directa.
    _, err = DB.NamedExec(`UPDATE tarjetas
		SET estatus=:estatus,  
			nomestatus=:nomestatus
		WHERE id=:id`, guiarem)
	
	// actualizar remesas 
	_,err = ActualizaRem(guiarem.Idremesa) 
	if err != nil {
		fmt.Println(err.Error())
		return
	}


	return

}
