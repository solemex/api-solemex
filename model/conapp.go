package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type PendxM struct {
	Idmensajero int `db:"idmensajero"    json:"idmensajero"`
}

type PriorxM struct {
	Idmensajero int `db:"idmensajero"    json:"idmensajero"`
}
type Pendientes struct {
	Id        		string `db:"id"     	   	json:"id"`
	Numguia   		string `db:"numguia"   		json:"numguia"`
	Prioridad 		int    `db:"prioridad"   	json:"prioridad"`
	Idcliente 		int    `db:"idcliente"   	json:"idcliente"`
	Idremesa  		int    `db:"idremesa"     	json:"idremesa"`
	Numrem    		string `db:"numrem"   		json:"numrem"`
	Estatus   		int    `db:"estatus" 	    json:"estatus"`
	Mensajero 		int    `db:"mensajero"    	json:"mensajero"`
	Email     		string `db:"email"        	json:"email"`
	Nomcli 	  		string `db:"nomcli"       	json:"nomcli"`
	Num_intentos  	int    `db:"num_intentos" 	json:"num_intentos"`
	Numsuc 			string `db:"numsuc"       	json:"numsuc"`
	Nomsuc    		string `db:"nomsuc"       	json:"nomsuc"`
	Colonia 		string `db:"colonia"      	json:"colonia"`
	Ciudad 			string `db:"ciudad"       	json:"ciudad"`
	Cp 				string `db:"cp"           	json:"cp"`
	Estado 			string `db:"estado"       	json:"estado"`
	Telefono 		string `db:"telefono"     	json:"telefono"`
	Nombanco 		string `db:"nombanco"       json:"nombanco"`
	Direccion 		string `db:"direccion"      json:"direccion"`
	Tipoent			string `db:"tipoent"        json:"tipoent"`


}

type Sucursal struct {
	Id        	int    `db:"id"     	   	json:"id"`
	Numsuc   	string `db:"numsuc"     	json:"numsuc"`
	Nomsuc    	string `db:"nomsuc"     	json:"nomsuc"`
	Direccion   string `db:"direccion"     	json:"direccion"`
	Colonia    	string `db:"colonia"     	json:"colonia"`
	Ciudad    	string `db:"ciudad"     	json:"ciudad"`
	Estado    	string `db:"estado"     	json:"estado"`
}

// func PendxMensajero(mensajero int) (pendientes []Pendientes, err error) {
// 	var DB = modeldb.GetDb()
// 	fmt.Println(mensajero)
// 	err = DB.Select(&pendientes, `SELECT tarjetas.id,
// 		COALESCE(tarjetas.idremesa, '0') as idremesa, 
// 		COALESCE(tarjetas.numrem,'') as numrem, 
// 		COALESCE(tarjetas.numguia,'') as numguia,
// 		COALESCE(tarjetas.nomcli, '') as nomcli,
// 		tarjetas.prioridad, 
// 		COALESCE (tarjetas.idcliente,'0') as idcliente, 
// 		COALESCE(tarjetas.email,'') as email,
// 		COALESCE(tarjetas.num_intentos, '0') as num_intentos
// 		FROM tarjetas
// 		WHERE estatus=1 AND mensajero=?`, mensajero)
// 	return
// }

func PendxMensajero(mensajero int) (pendientes []Pendientes, err error) {
	var DB = modeldb.GetDb()
	fmt.Println(mensajero)
	err = DB.Select(&pendientes, `SELECT tarjetas.id,
        COALESCE(tarjetas.idremesa, '0') as idremesa, 
        COALESCE(tarjetas.numrem,'') as numrem, 
        COALESCE(tarjetas.numguia,'') as numguia,
        COALESCE(tarjetas.nomcli, '') as nomcli,
        COALESCE(tarjetas.prioridad, '0') as prioridad, 
        COALESCE (tarjetas.idcliente,'0') as idcliente,  
        COALESCE(tarjetas.email,'') as email,
        COALESCE(tarjetas.num_intentos, '0') as num_intentos,

        COALESCE(sucursales.numsuc, '')   as numsuc,
        COALESCE(sucursales.nomsuc, '')   as nomsuc ,
        COALESCE(tarjetas.mensajero, '0') as mensajero,
        COALESCE(tarjetas.estatus, '0' )  as estatus,

        COALESCE(sucursales.colonia, '')  as colonia,
        COALESCE(sucursales.ciudad, '')   as ciudad, 
        COALESCE(sucursales.cp, '' )      as cp, 
        COALESCE(sucursales.estado, '')   as estado,
        COALESCE(sucursales.telefono, '') as telefono,
        COALESCE(sucursales.direccion,'') as direccion ,
        COALESCE(clientes.nomcli) as nombanco,
        COALESCE(tarjetas.tipoent) as tipoent
        FROM tarjetas
        INNER JOIN sucursales ON tarjetas.idcliente = sucursales.idcliente
        INNER JOIN clientes   ON tarjetas.idcliente = clientes.id
        WHERE estatus = 1 AND mensajero=?  AND sucursales.idcliente = tarjetas.idcliente AND tarjetas.idsucbr = sucursales.numsuc
        OR estatus = 4 AND mensajero=?  AND sucursales.idcliente = tarjetas.idcliente AND tarjetas.idsucbr = sucursales.numsuc`, mensajero, mensajero)
	return
}

func PrioridadxMens(mensajero int) (pendientes []Pendientes, err error) {
	var DB = modeldb.GetDb()
	fmt.Println(mensajero)
	err = DB.Select(&pendientes, `SELECT tarjetas.id,
	COALESCE (tarjetas.idremesa, '0') as idremesa, 
	COALESCE (tarjetas.numrem,'') as numrem, 
	COALESCE (tarjetas.numguia,'') as numguia,
	tarjetas.prioridad, tarjetas.idcliente
	FROM tarjetas
	WHERE id > 20000 AND prioridad=1 AND mensajero=?`, mensajero)
	return
}


func GetSuc(id string) (sucursal Sucursal, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&sucursal, `SELECT id, numsuc, nomsuc, direccion, colonia, ciudad, estado FROM sucursales WHERE idcliente=?`, id)
	return
}
