
package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)


type LoginApp struct {
	ID            int    `db:"id"     	 	 json:"id"`
	Nombre        string `db:"nombre"    	 json:"nombre"`
	Nomuser       string `db:"nomuser"	  	 json:"nomuser"`
	Estatus       string `db:"estatus"     	 json:"estatus"`
	Idmensajeria  int    `db:"idmensajeria"        json:"idmensajeria"`
	Nommensajeria string `db:"nommensajeria"     json:"nommensajeria"`
}

type LoginPar struct {
	Nomuser  string `db:"nomuser"	  	 json:"nomuser"`
	Password string `db:"password"     	 json:"password"`
}


func LoginAppMovil(nomuser string, password string) (loginapp []LoginApp, err error) {
	var DB = modeldb.GetDb()
	fmt.Println("nomuser, password ", nomuser, password)

	err = DB.Select(&loginapp, `SELECT  mensajeros.id, 
		mensajeros.nombre, mensajeros.nomuser,
		coalesce(mensajerias.nommensajeria, '') as nommensajeria, 
		coalesce(mensajerias.id, 0) as idmensajeria, mensajeros.estatus
		FROM mensajeros LEFT JOIN mensajerias on mensajeros.idmensajeria = mensajerias.id
		WHERE mensajeros.estatus = 2 and mensajeros.nomuser = ? 
		AND mensajeros.password = ? `, nomuser, password)
	return
}
