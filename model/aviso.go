package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
	//"github.com/jmoiron/sqlx"
	"fmt"
)

type Avisos struct {
	Id          string `db:"id"     	 	 json:"id"`
	Idguia      int    `db:"idguia"    	 	 json:"idguia"`
	Idmensajero int    `db:"idmensajero"	 json:"idmensajero"`
	Fecha       string `db:"fecha"     		 json:"fecha"` 
	Hora        string `db:"hora"     		 json:"hora"`   
	Aviso       string `db:"aviso"  	     json:"aviso"` 
	Numguia     string `db:"numguia"  	     json:"numguia"`
	Idusuario   int    `db:"idusuario"		 json:"idusuario"`
	Estatus     int    `db:"estatus"     	 json:"estatus"`
	Fecha_visto string `db:"fecha_visto"     json:"fecha_visto"` 
	Hora_visto  string `db:"hora_visto"   	 json:"hora_visto"`   
}

type Avisosxm struct {
	Idmensajero int `db:"idmensajero"	 json:"idmensajero"`
}

type Prioridad struct {
	Numguia string `db:"numguia"  	     json:"numguia"`
	Prioridad int  `db:"prioridad"  	 json:"prioridad"`
}


// Add Avisos put
func AddPrioridad(prioridad Prioridad) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE tarjetas
		SET prioridad=:prioridad
		WHERE numguia=:numguia or numcli=:numguia`, &prioridad)
	return
}



func GetAvisosxM(idmensajero int) (avisos []Avisos, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&avisos, `SELECT id, numguia, idmensajero, idguia, fecha, hora, aviso 
		FROM avisos 
		WHERE idmensajero=?`, idmensajero)
	fmt.Println("BuscarIdKey =", err)
	return
}

//TRAER TODO
func ListAvisos() (avisos []Avisos, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&avisos, `SELECT id, idguia, idmensajero, fecha, hora, aviso, idusuario,
			estatus, fecha_visto, hora_visto
			FROM avisos`)
	return
}

//POST || INSERTAR
func InsertAvisos(avisos Avisos) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO avisos ( idguia, numguia, idmensajero, fecha, hora, aviso, idusuario)
        VALUES( :idguia, :numguia, :idmensajero, :fecha, :hora, :aviso, :idusuario)`, &avisos)
	return
}

//POST || CONSULTA CON PARAMETROS
func InsertAviso(avisos Avisos) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO avisos 
		( idguia, numguia, idmensajero, fecha, hora, aviso, idusuario)
        VALUES( :idguia, :numguia, :idmensajero, :fecha, :hora, :aviso, :idusuario)`, &avisos)
	return
}

//PARAMETROS: idguia, idmensajero
func BusIdAvisos(idguia int, idmensajero int) (id Avisos, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&id, `SELECT id FROM avisos 
		WHERE idguia=? AND idmensajero=?`, idguia, idmensajero)
	fmt.Println("BuscarIdKey =", err)
	return
}

func UpdateAvisos(avisos Avisos) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE avisos
        SET idguia=:idguia, numguia=:numguia, idmensajero=:idmensajero,
        	aviso=:aviso, idusuario=:idusuario, fecha=:fecha, hora=:hora
        WHERE idguia=:idguia AND idmensajero=:idmensajero`, avisos)

	return
}
