package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type Rastreo struct {
	Mensajero 		 int    `db:"mensajero"     			json:"mensajero"`
	Numguia          string `db:"numguia"     			json:"numguia"`
	Numcli           string `db:"numcli"  	   			json:"numcli"`
	Fingreso         string `db:"fingreso"     			json:"fingreso"`
	Fcierre          string `db:"fcierre"  	   			json:"fcierre"`
	Nomcli           string `db:"nomcli"   	 	  		json:"nomcli"`
	Fecha_print      string `db:"fecha_print"   		json:"fecha_print"`
	Hora_print       string `db:"hora_print"   			json:"hora_print"`
	Fecha_captura    string `db:"fecha_captura" 		json:"fecha_captura"`
	Fecha_asignacion string `db:"fecha_asignacion"  json:"fecha_asignacion"`
	Hora_asignacion  string `db:"hora_asignacion"   json:"hora_asignacion"`
	Cant             int    `db:"cant"     					json:"cant"`
	Estatus          int    `db:"estatus" 		 			json:"estatus"`
	Nomestatus       string `db:"nomestatus" 	  		json:"nomestatus"`
	Fecha_entrega    string `db:"fecha_entrega" 		json:"fecha_entrega"`
	Hora_entrega     string `db:"hora_entrega"   		json:"hora_entrega"`
	Nomrecibe        string `db:"nomrecibe"   	 		json:"nomrecibe"`
	Parentesco       string `db:"parentesco"   	 		json:"parentesco"`
	Nombre           string `db:"nombre"  	   	 		json:"nombre"`
	Fvenc            string `db:"fvenc"  	   	 	  	json:"fvenc"`
	Num_intentos     int    `db:"num_intentos"    	json:"num_intentos"`
	Latitude         string `db:"latitude"      		json:"latitude"`
	Longitude        string `db:"longitude"     		json:"longitude"`
	Prioridad        int    `db:"prioridad"       	json:"prioridad"`
	Nomsuc 					 string `db:"nomsuc"     		    json:"nomsuc"`	
	Direccion 			 string `db:"direccion"     		json:"direccion"`
	Cp 							 string `db:"cp"     		        json:"cp"`
	Colonia          string `db:"colonia"     		  json:"colonia"`
	Ciudad	         string `db:"ciudad"     		    json:"ciudad"`
	Estado		       string `db:"estado"     		    json:"estado"`
	Telefono	       string `db:"telefono"     		  json:"telefono"`	
	Tipoentrega      string `db:"tipo_entrega"      json:"tipo_entrega,omitempty"`
}

type Intentos struct {
	Id           int    `db:"id"    	  		json:"id"`
	Numguia      string `db:"numguia"   		json:"numguia"`
	Num_intentos int    `db:"num_intentos"  json:"num_intentos"`
	Latitude     string `db:"latitude"   		json:"latitude"`
	Longitude    string `db:"longitude"  		json:"longitude"`
	Motivo       string `db:"motivo"    		json:"motivo"`
	Comentarios  string `db:"comentarios"  	json:"comentarios"`
	Fecha        string `db:"fecha"   			json:"fecha"` //DATE
	Hora         string `db:"hora"   			  json:"hora"`
	Mensajero    int    `db:"mensajero"     json:"mensajero"`
	Remesa       string `db:"remesa"   			json:"remesa"`

}

func RastreoIntentos(numguia string) (intentos []Intentos, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&intentos, `SELECT  id, numguia,
		COALESCE(num_intentos, 0)  		as num_intentos,
	  COALESCE(latitude,'')      		as latitude,
		COALESCE(longitude,'')     		as longitude,
	  COALESCE(motivo,'')        		as motivo,
	  COALESCE(comentarios,'')      as comentarios,
		COALESCE(fecha,'')            as fecha, 
	  COALESCE(hora,'')             as hora, 
		COALESCE(mensajero, 0 )       as mensajero,
		COALESCE(remesa,'')           as remesa
	FROM intentos 
	WHERE numguia=?`, numguia)
	return
}


func RastreoTarjxSuc(numguia string) (rastreo []Rastreo, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&rastreo, `SELECT  numguia, mensajero,
		COALESCE(tarjetas.numcli,'') as numcli,
	  COALESCE(nomcli,'')           as nomcli,
		COALESCE(fecha_print,'')      as fecha_print,
	  COALESCE(hora_print,'')       as hora_print,
	  COALESCE(fecha_captura,'')    as fecha_captura,
		COALESCE(fecha_asignacion,'') as fecha_asignacion, 
	  COALESCE(hora_asignacion,'')  as hora_asignacion, 
		COALESCE(tarjetas.cant,1)     as cant,
		COALESCE(fecha_entrega,'')    as fecha_entrega, 
		COALESCE(hora_entrega,'')     as hora_entrega, 
	  COALESCE(nomrecibe,'')        as nomrecibe, 
	  COALESCE(parentesco,'')       as parentesco,
	  mensajeros.nombre             as nombre, 
	  COALESCE(latitude,'')         as latitude, 
	  COALESCE(longitude,'')        as longitude,
	  tarjetas.estatus,
	  tarjetas.tipoent,
	  tarjetas.nomestatus,
	  tarjetas.num_intentos,
	  tarjetas.prioridad,
	  COALESCE(remesas.fvenc,'')    as fvenc,
	  COALESCE(remesas.fingreso,'') as fingreso, 
	  COALESCE(remesas.fcierre, '') as fcierre,
	  sucursales.nomsuc,
	  sucursales.direccion,
	  sucursales.cp,
	  sucursales.colonia,
	  sucursales.ciudad,
	  sucursales.estado,
	  sucursales.telefono
	FROM tarjetas LEFT JOIN mensajeros ON mensajeros.id      = tarjetas.mensajero 
				        LEFT JOIN remesas    ON tarjetas.idremesa  = remesas.id 
				        LEFT JOIN sucursales ON tarjetas.idcliente = sucursales.idcliente 
	WHERE numguia=? and tarjetas.idsucbr = sucursales.numsuc and tarjetas.idcliente = sucursales.idcliente`, numguia)
	return
}

// PARAMETROS: numgstring `db:"fecha_captura"   	 json:"fecha_captura"` uia
func RastreoTarjeta(numguia string) (rastreo []Rastreo, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&rastreo, `SELECT  numguia, mensajero,
		COALESCE(tarjetas.numcli,'') 	as numcli,
		COALESCE(remesas.fingreso,'') as fingreso, 
	  coalesce(remesas.fcierre, '') as fcierre,
	  COALESCE(remesas.fvenc,'') 		as fvenc,
	  COALESCE(nomcli,'') 					as nomcli,
		COALESCE(fecha_print,'') 		  as fecha_print,
	  COALESCE(hora_print,'') 		  as hora_print,
	  COALESCE(fecha_captura,'') 		as fecha_captura,
		COALESCE(fecha_asignacion,'') as fecha_asignacion, 
	  COALESCE(hora_asignacion,'')  as hora_asignacion, 
		coalesce(tarjetas.cant,1)     as cant,
	  COALESCE(fecha_entrega,'')    as fecha_entrega, 
		COALESCE(hora_entrega,'')     as hora_entrega, 
	  COALESCE(nomrecibe,'')        as nomrecibe, 
	  COALESCE(parentesco,'') 		  as parentesco,
	  coalesce(latitude,'') 		    as latitude, 
	  mensajeros.nombre  						as nombre, 
	  coalesce(longitude,'')        as longitude,
	  tarjetas.num_intentos,
	  tarjetas.nomestatus,
		tarjetas.estatus,
	  COALESCE(tarjetas.tipo_entrega, 0) as tipo_entrega,
	  tarjetas.prioridad,
	  tarjetas.colonia,
	  tarjetas.ciudad,
	  tarjetas.cp,
	  tarjetas.estado
	FROM tarjetas LEFT JOIN mensajeros ON mensajeros.id= tarjetas.mensajero 
				  LEFT JOIN remesas ON tarjetas.idremesa = remesas.id 
	WHERE numguia=?`, numguia)
	return
}
