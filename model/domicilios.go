package model

import (
	"fmt"
	"gitlab.com/solemex/api-solemex/modeldb"
	_ "github.com/go-sql-driver/mysql"
)

type Domicilios struct {
	ID          string `db:"id"     	 json:"id"`
	Nomcont		string `db:"nomcont"   	 json:"nomcont"`
	Direccion   string `db:"direccion"   json:"direccion"`
	Numext      string `db:"numext"   	 json:"numext"`  	
	Cp 			string `db:"cp"   	 	 json:"cp"`
	Colonia		string `db:"colonia"   	 json:"colonia"`
	Ciudad 		string `db:"ciudad"   	 json:"ciudad"`
	Estado		string `db:"estado"   	 json:"estado"`
	Pais		string `db:"pais"   	 json:"pais"`
	Tel1 		string `db:"tel1"  	 	 json:"tel1"`
	Tel2		string `db:"tel2"  	 	 json:"tel2"`
	Tel3 		string `db:"tel3"  	 	 json:"tel3"`
	
}



func ListDomicilios() (domicilios []Domicilios, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&domicilios, `SELECT id, COALESCE(nomcont,'') as nomcont, direccion, numext, cp, colonia, ciudad, 
			estado, pais, tel1, tel2, tel3, numuser 
			FROM domicilios WHERE id > 20000` )
	return
}

	// GET DOMICILIOS By ID
func GetDomicilios(id string) (domicilios Domicilios, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&domicilios, `SELECT id, COALESCE(nomcont,'') as nomcont, direccion, numext, cp, colonia, ciudad, 
			estado, pais, tel1, tel2, tel3, numuser 
			FROM domicilios WHERE id=?`, id)
	return
}



func InsertDomicilios (domicilios Domicilios) ( err error) {
	var DB = modeldb.GetDb()

	_,err = DB.NamedExec(`INSERT INTO domicilios(nomcont, direccion,  cp, colonia, ciudad, estado, pais, tel1, tel2, tel3)
		VALUES( :nomcont, :direccion, :cp, :colonia, :ciudad, :estado, :pais, :tel1, :tel2, :tel3) `, &domicilios)

	
	if err != nil {
	    panic(err)
	}
	fmt.Println("LastInsertId: ", err)

	return
}


// func Getiddom () (domicilios []Domicilios, err error) {
// 	var DB = modeldb.GetDb()
// 	err = DB.Select(&domicilios,`SELECT id FROM bajopuerta.domicilios ORDER BY id desc limit 1` )
// 	return
// }
