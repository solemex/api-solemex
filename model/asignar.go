package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"

)

type Asignar struct {
	Id               int    `db:"id"     	   	    json:"id"`
	Numguia          string `db:"numguia"   	   	json:"numguia"`
	Numcli 			 string `db:"numcli"     		json:"numcli"`
	Mensajero        int    `db:"mensajero"     	json:"mensajero"`
	Fecha_asignacion string `db:"fecha_asignacion"  json:"fecha_asignacion"` //DATE
	Hora_asignacion  string `db:"hora_asignacion"   json:"hora_asignacion"`
	User_asignacion  int    `db:"user_asignacion" 	json:"user_asignacion"`
	Idremesa         int    `db:"idremesa" 	json:"idremesa"`
}


type Emailxid struct {
	Id      string  `db:"id"      json:"id"`
	Nomcli	string  `db:"nomcli"  json:"nomcli"`
	Email  string   `db:"email"   json:"email"`
}




// GET TARJETASA By ID
func GetEmailxId(id string) (emailxid Emailxid, err error) {
	var DB = modeldb.GetDb()
	
	err = DB.Get(&emailxid, `SELECT id,  COALESCE(nomcli,'') as nomcli,
		COALESCE(email,'') as email
	FROM tarjetas
	   WHERE id=?`, id)
	return
}




//PARAMETROS: numguia, codigo
func BusIdAsignar(numguia string) (id Asignar, err error) {
	var DB = modeldb.GetDb()
	fmt.Println(numguia)
	err = DB.Get(&id, `SELECT id, idremesa FROM tarjetas 
		WHERE numguia=? or numcli=?`, numguia, numguia)
	return
}

func UpdateAsignar(asignar Asignar) (err error) {
	var DB = modeldb.GetDb()

	_, err = DB.NamedExec(`UPDATE tarjetas
		SET mensajero=:mensajero,  fecha_asignacion=:fecha_asignacion,
			hora_asignacion=:hora_asignacion,
		    user_asignacion=:user_asignacion
		WHERE id=:id`, asignar)
	//fmt.Println(asignar)
	return
}


type Resul struct {
	Id       int `db:"id"       json:"id"`
	Na       int `db:"na"       json:"na"`
	Asig     int `db:"asig"     json:"asig"`
	En    	 int `db:"en"      	json:"en"`
	Dr    	 int `db:"dr"      	json:"dr"`
	Ex    	 int `db:"ex"      	json:"ex"`
	Tot      int `db:"tot"      json:"tot"` 
}

func ActualizaRem (idrem int) (resul Resul, err error) {
	var DB = modeldb.GetDb()
	
	// Meter al arreglo el param de la id remesas
	resul.Id = idrem

	// Calcular NO Asignadas
 	 err = DB.Get(&resul, `SELECT COUNT(mensajero) as asig 
		from tarjetas 
		where tarjetas.mensajero <> 0 
		and idremesa= ? `, idrem)

	// Calcular Asignadas
 	 err = DB.Get(&resul, `SELECT COUNT(mensajero) as na 
		from tarjetas 
		where tarjetas.mensajero = 0 and tarjetas.adicional = 0
		and idremesa= ? `, idrem)

	// Calcular Entregadas
	err = DB.Get(&resul, `SELECT COUNT(estatus) as en
		from tarjetas 
		where tarjetas.estatus = 3 
		and idremesa= ? `, idrem)

	// Calcular Devueltas. 
	err = DB.Get(&resul, `SELECT COUNT(estatus) as dr
		from tarjetas 
		where tarjetas.estatus = 4 
		and idremesa = ? `, idrem)

	// Calcular Adicionales.  Ex 
	err = DB.Get(&resul, `SELECT SUM(tarjetas.adicional) as ex
		from tarjetas 
		where   idremesa = ? `, idrem)

// err = DB.Get(&resul, `SELECT tot from remesas 
// 		where remesas.id = ? `, idrem)
	

	// Saber el TOTAL de tarjetas
	err = DB.Get(&resul, `SELECT COUNT(estatus) as tot
		from tarjetas 
		WHERE tarjetas.numguia is NOT NULL
		and idremesa = ? `, idrem)


	// Calcular Efectividad. 
	// fmt.Println("Asign=" , resul.En, "Total ", resul.Tot, "Adicionales", resul.Ex )
	// if resul.Tot > 0 {
	// 	Efe :=  (resul.En / resul.Tot )
	// 	fmt.Println ("Efectividad ", Efe)	
	// }
	
	// Actualizar
	_, err = DB.NamedExec(`UPDATE remesas
			SET na=:na,  
				asig=:asig,
				en=:en,
			    dr=:dr,
			    di=:dr,
			    ex=:ex,
			    tot=:tot
			WHERE id=:id`, resul)
	return 

}

