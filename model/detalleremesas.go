package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

//Reporte de salida
type Detalle struct {
	Numrem        string `db:"numrem"   	   	 json:"numrem"`
	Entregadas    string `db:"entregadas"   	 json:"entregadas"`
	Fecha_entrega string `db:"fecha_entrega"   	 json:"fecha_entrega"`
	Avance        string `db:"avance"   	     json:"avance"`
}

type Numrem struct {
	Numrem        string `db:"numrem"   	   	 json:"numrem"`
}

func DetalleRemesas(numrem string) (detalle []Detalle, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&detalle, `SELECT numrem, count(estatus) AS "entregadas", fecha_entrega, count(estatus) /  (SELECT count(estatus) AS "Total" FROM tarjetas
		WHERE numrem=?) * 100 AS "avance" FROM tarjetas
		WHERE estatus = 3 AND numrem=?
		GROUP BY numrem, estatus, fecha_entrega`, numrem, numrem)
	return
}
