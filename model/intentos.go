package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type SupervisionInt struct {
	Mensajero     int    `db:"mensajero"     json:"mensajero"`
	Numguia       string `db:"numguia"   	 json:"numguia"`
	Fecha         string `db:"fecha"         json:"fecha"` 
	Hora          string `db:"hora"          json:"hora"`
	Latitude      string `db:"latitude"   	 json:"latitude"`
	Longitude     string `db:"longitude"   	 json:"longitude"`
	Motivo 	      string `db:"motivo"   	 json:"motivo"`
}

func GetSuperInt(mensajero int , fecha string) (supervisionint []SupervisionInt, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&supervisionint, `SELECT  numguia, mensajero, motivo,
	     COALESCE(latitude,'') as latitude , COALESCE(longitude,'')  as longitude, 
       COALESCE(fecha,'')      as fecha    , COALESCE(hora,'')       as hora
	FROM intentos WHERE mensajero=? AND fecha =? `, mensajero, fecha)
	return
}