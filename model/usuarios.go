package model

import (
	"gitlab.com/solemex/api-solemex/modeldb"
	_ "github.com/go-sql-driver/mysql"
)

type Usuarios struct {
	ID        string `db:"id"     	 	 json:"id"`
	Nomuser   string `db:"nomuser"    	 json:"nomuser"`
	Apellido  string `db:"apellido"	  	 json:"apellido"`
	Password  string `db:"password"      json:"password"`
	Email     string `db:"email"         json:"email"`
	Numsuc    int    `db:"numsuc"  	     json:"numsuc"`
	Empresa   string `db:"empresa"		 json:"empresa"`
	Telefono  string `db:"telefono"      json:"telefono"`
	Rfc       string `db:"rfc"       	 json:"rfc"`
	Estatus   string `db:"estatus"       json:"estatus"`
	Fecha     string `db:"fecha"		 json:"fecha"`   //DATE
	Hora      string `db:"hora"     	 json:"hora"` //TIME
	Codigo    string `db:"codigo"        json:"codigo"`
	Nivel     int    `db:"nivel"	     json:"nivel"`
	Admin     int    `db:"admin"		 json:"admin"` //TINYINT
	IDCliente int    `db:"idcliente"	 json:"idcliente"`
	Contra    string `db:"contra"     	 json:"contra"`
}

func ListCatUsuarios() (usuarios []Usuarios, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&usuarios, `SELECT  id, nomuser, apellido, password,
			email, numsuc, empresa, telefono, rfc, estatus, fecha,
			hora, codigo, nivel, admin, idcliente, contra FROM usuarios`)
	return
}

func GetCatUsuarios(id string) (usuarios Usuarios, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&usuarios, `SELECT  id, nomuser, apellido, password,
			email, numsuc, empresa, telefono, rfc, estatus, fecha,
			hora, codigo, nivel, admin, idcliente, contra FROM usuarios
		WHERE id=?`, id)
	return
}

func InsertCatUsuarios(usuarios Usuarios) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO usuarios ( nomuser, apellido,
			password, email, numsuc, empresa, telefono, rfc, estatus, fecha,
			hora, codigo, nivel, admin, idcliente, contra )
		VALUES(:nomuser,  :apellido, :password, :email,
			   :numsuc, :empresa, :telefono :rfc, :estatus, :fecha, :hora,
			   :codigo, :nivel, :admin, :idcliente, :contra )`, &usuarios)
	return
}

func UpdateCatUsuarios(usuarios Usuarios) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE usuarios
		SET nomuser=:nomuser, apellido=:apellido, password=:password, email=:email,
			numsuc=:numsuc, empresa=:empresa, telefono=:telefono, rfc=:rfc, estatus=:estatus,
		    fecha=:fecha,  hora=:hora, codigo=:codigo,
		    nivel=:nivel, admin=:admin, idcliente=:idcliente, contra=:contra,
		WHERE id=:id`, usuarios)
	return
}
