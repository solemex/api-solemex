package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type ConTarjetas struct {
	Id         string `db:"id"     	   	     json:"id"`
	Numguia    string `db:"numguia"   	   	 json:"numguia"`
	Idremesa   int    `db:"idremesa"      	 json:"idremesa"`
	Numrem     string `db:"numrem"   	   	 json:"numrem"`
	Numcli     string `db:"numcli"  	   	 json:"numcli"`
	Nomcli     string `db:"nomcli"  	   	 json:"nomcli"`
	Nomestatus string `db:"nomestatus"   	 json:"nomestatus"`
	Idcliente  int    `db:"idcliente"   	 json:"idcliente"`
	Idservicio int    `db:"idservicio"   	 json:"idservicio"`
}

func ConPendTodas(idcliente int, idservicio int) (contarjetas []ConTarjetas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&contarjetas, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, nomestatus
				FROM tarjetas WHERE idcliente=? AND idservicio=? AND estatus=1`, idcliente, idservicio)
	return
}

func ConPendAsig(idcliente int, idservicio int) (contarjetas []ConTarjetas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&contarjetas, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, nomestatus
				FROM tarjetas
				WHERE idcliente=? AND idservicio=? AND estatus=1 AND mensajero > 0`, idcliente, idservicio)
	return
}

func ConPendNoAsig(idcliente int, idservicio int) (contarjetas []ConTarjetas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&contarjetas, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, nomestatus
				FROM tarjetas
				WHERE idcliente=? AND idservicio=? AND estatus=1 AND mensajero = 0`, idcliente, idservicio)
	return
}

func ConEntregadas(idcliente int, idservicio int) (contarjetas []ConTarjetas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&contarjetas, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, nomestatus
				FROM tarjetas
				WHERE idcliente=? AND idservicio=? AND estatus=3`, idcliente, idservicio)
	return
}

func ConDevueltas(idcliente int, idservicio int) (contarjetas []ConTarjetas, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&contarjetas, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, nomestatus
				FROM tarjetas
				WHERE idcliente=? AND idservicio=? AND estatus=4`, idcliente, idservicio)
	return
}
