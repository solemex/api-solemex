package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)


type Getid struct {
	IDusuariosweb string `db:"idusuariosweb"    json:"idusuariosweb"`
	Email         string `db:"email"	  	    json:"email"`
}

type Usuario struct{
	Email         string `db:"email"	  	    json:"email"`
	Password      string `db:"password"    		json:"password"`
}

type UsuariosWeb struct {
	IDusuariosweb string `db:"idusuariosweb"    json:"idusuariosweb"`
	Nomuser       string `db:"nomuser"  	    json:"nomuser"`
	Email         string `db:"email"	  	    json:"email"`
	Telefono      string `db:"telefono"    		json:"telefono"`
	Celular       string `db:"celular"   		json:"celular"`
	Empresa       string `db:"empresa"    		json:"empresa"`
	Depto         string `db:"depto"   			json:"depto"`
	Suc           string `db:"suc"	  			json:"suc"`
	Numcli        string `db:"numcli"   		json:"numcli"`
}


type BuscarUsr struct {
	IDusuariosweb string `db:"idusuariosweb"    json:"idusuariosweb"`
	Nomuser       string `db:"nomuser"  	    json:"nomuser"`
	Email         string `db:"email"	  	    json:"email"`
	Password      string `db:"password"    		json:"password"`
	Numcli        string `db:"numcli"   		json:"numcli"`
    Nivel         string `db:"nivel"   		json:"nivel"`
}

type Userweb struct {
	IDusuariosweb string `db:"idusuariosweb"  json:"idusuariosweb"`
	Nomuser       string `db:"nomuser"  	    json:"nomuser"`
	Email         string `db:"email"	  	    json:"email"`
	// Password      string `db:"password"    		json:"password"`
	Numcli        string `db:"numcli"   		  json:"numcli"`
	Telefono      string `db:"telefono"    		json:"telefono"`
	Celular       string `db:"celular"   		  json:"celular"`
	Depto         string `db:"depto"   			  json:"depto"`
	Nivel         string `db:"nivel"   		    json:"nivel"`
	Idnivel 	    string `db:"idnivel"   		  json:"idnivel"`
	Idcliente 	  string `db:"idcliente"   		json:"idcliente"`
	Nomcli        string `db:"nomcli"   		  json:"nomcli"`
	Idmensajeria  string `db:"idmensajeria"   json:"idmensajeria"`
	Nommensajeria string `db:"nommensajeria"  json:"nommensajeria"`
}

type InsertUsuariosW struct {
	Nomuser       string `db:"nomuser"  	    json:"nomuser"`
	Email         string `db:"email"	  	    json:"email"`
	Telefono      string `db:"telefono"    		json:"telefono"`
	Celular       string `db:"celular"   		json:"celular"`
	Empresa       string `db:"empresa"    		json:"empresa"`
	Depto         string `db:"depto"   			json:"depto"`
	Suc           string `db:"suc"	  			json:"suc"`
	Password      string `db:"password"    		json:"password"`
	Numcli        string `db:"numcli"   		json:"numcli"`
	Nivel         string `db:"nivel"   		  json:"nivel"`
	Estatus       string `db:"estatus"   		json:"estatus"`
	Idnivel       int    `db:"idnivel"   	  json:"idnivel"`
}


type Activar struct {
	Idusuariosweb string `db:"idusuariosweb"    json:"idusuariosweb"`
	Estatus int  `db:"estatus"    json:"estatus"`

}




func ActivarUsuario(activar Activar) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE usuariosweb
		SET estatus= 2
		WHERE idusuariosweb=:idusuariosweb`, activar)
	return
}


// Para validar login
func GetidxMail (email string) (getid Getid, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&getid, `SELECT idusuariosweb
		FROM usuariosweb WHERE email = ?`, email)
		fmt.Println(getid)
	return 
}

// func ListUsuariosWeb() (usuariosweb []GetUserWeb, err error) {
// 	var DB = modeldb.GetDb()
// 	err = DB.Select(&usuariosweb, `SELECT idusuariosweb, nomuser, email, telefono, celular,
// 			empresa, depto, suc, password, numcli, nivel FROM usuariosweb`)
// 	return
// }


type GetUsers struct {
	IDusuariosweb string `db:"idusuariosweb"  json:"idusuariosweb"`
	Nomuser       string `db:"nomuser"  	    json:"n omuser"`
	Email         string `db:"email"	  	    json:"email"`
  Nivel         string `db:"nivel"   			  json:"nivel"`
  Idnivel 	    string `db:"idnivel"   		  json:"idnivel"`
	Idcliente 	  int    `db:"idcliente"   		json:"idcliente"`
	Nomcli        string `db:"nomcli"   		  json:"nomcli"`
	Idmensajeria  int    `db:"idmensajeria"   json:"idmensajeria"`
	Nommensajeria string `db:"nommensajeria"  json:"nommensajeria"`
  Estatus 	    string `db:"estatus"   		  json:"estatus"`
}


func ListCatUsr() (getusers []GetUsers, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&getusers, `SELECT 	u.idusuariosweb, u.nomuser, u.email, 
		COALESCE(n.nivel, "") AS nivel  ,
		COALESCE( u.idnivel, 0) AS idnivel, 
		COALESCE (u.estatus, 0) AS estatus, 
		COALESCE( u.idcliente, 0) AS idcliente,
	  COALESCE( u.idmensajeria, 0) AS idmensajeria,
	  COALESCE (clientes.nomcli,'') as nomcli, 
	  COALESCE (mensajerias.nommensajeria, '') as nommensajeria 
		FROM usuariosweb u 
			left join niveles n on u.idnivel = n.id  
			left join clientes on u.idcliente = clientes.id 
			left join mensajerias on u.idmensajeria = mensajerias.id`)
	return
}

// Consulta de usuarios por mensajería 
func ListUsuariosxMia(id int) (getusers []GetUsers, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&getusers, `SELECT 	u.idusuariosweb, u.nomuser, u.email, 
		COALESCE(n.nivel, "") AS nivel  ,
		COALESCE( u.idnivel, 0) AS idnivel, 
		COALESCE (u.estatus, 0) AS estatus, 
		COALESCE( u.idcliente, 0) AS idcliente,
	  COALESCE( u.idmensajeria, 0) AS idmensajeria,
	  COALESCE (clientes.nomcli,'') as nomcli, 
	  COALESCE (mensajerias.nommensajeria, '') as nommensajeria 
		FROM usuariosweb u 
			left join niveles n on u.idnivel = n.id  
			left join clientes on u.idcliente = clientes.id 
			left join mensajerias on u.idmensajeria = mensajerias.id
		WHERE u.idmensajeria = ?`, id)
	return
}

// Consulta de usarios por cliente
func ListUsuariosxCli(id int) (getusers []GetUsers, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&getusers, `SELECT 	u.idusuariosweb, u.nomuser, u.email, 
		COALESCE(n.nivel, "") AS nivel  ,
		COALESCE( u.idnivel, 0) AS idnivel, 
		COALESCE (u.estatus, 0) AS estatus, 
		COALESCE( u.idcliente, 0) AS idcliente,
	  COALESCE( u.idmensajeria, 0) AS idmensajeria,
	  COALESCE (clientes.nomcli,'') as nomcli, 
	  COALESCE (mensajerias.nommensajeria, '') as nommensajeria 
		FROM usuariosweb u 
			left join niveles n on u.idnivel = n.id  
			left join clientes on u.idcliente = clientes.id 
			left join mensajerias on u.idmensajeria = mensajerias.id
		WHERE u.idcliente = ?`, id)
	return
}


type PutUsuariosWeb struct {
	IDusuariosweb string `db:"idusuariosweb" json:"idusuariosweb"`
	Nomuser       string `db:"nomuser"  	   json:"nomuser"`
	Email         string `db:"email"	  	   json:"email"`
	Telefono      string `db:"telefono"    	 json:"telefono"`
	Celular       string `db:"celular"   	   json:"celular"`
	Empresa       string `db:"empresa"    	 json:"empresa"`
	Depto         string `db:"depto"   		   json:"depto"`
	Suc           string `db:"suc"	  		   json:"suc"`
	Numcli        string `db:"numcli"   	   json:"numcli"`
	Nivel         string `db:"nivel"   	     json:"nivel"`
  Idnivel 	    int    `db:"idnivel"       json:"idnivel"`
	Idcliente 	  int    `db:"idcliente"   	 json:"idcliente"`
	Nomcli        string `db:"nomcli"        json:"nomcli"`
	Idmensajeria  int    `db:"idmensajeria"  json:"idmensajeria"`
	Nommensajeria string `db:"nommensajeria" json:"nommensajeria"`
  Estatus       string `db:"estatus"       json:"estatus"`
	Password      string `db:"password"    		json:"password"`

}

func UpdateUsuariosWeb(putusuariosweb PutUsuariosWeb) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE usuariosweb
		SET nomuser=:nomuser, email=:email, telefono=:telefono, celular=:celular,
			empresa=:empresa, depto=:depto, suc=:suc, idcliente=:idcliente, 
			idnivel=:idnivel,idmensajeria=:idmensajeria, estatus=:estatus, password=:password
		WHERE idusuariosweb=:idusuariosweb`, putusuariosweb)
	return
}

// func UpdateUsuariosWeb(putusuariosweb PutUsuariosWeb) (err error) {
// 	var DB = modeldb.GetDb()
// 	_, err = DB.NamedExec(`UPDATE usuariosweb
// 		SET nomuser=:nomuser, email=:email, telefono=:telefono, celular=:celular,
// 			empresa=:empresa, depto=:depto, suc=:suc,
// 			idcliente=:idcliente, idnivel=:idnivel, idmensajeria=:idmensajeria,
// 			estatus=:estatus
// 		WHERE idusuariosweb=:idusuariosweb`, putusuariosweb)
// 	return
// }



type GetUserWeb struct {
	IDusuariosweb string `db:"idusuariosweb"    json:"idusuariosweb"`
	Nomuser       string `db:"nomuser"  	    json:"nomuser"`
	Email         string `db:"email"	  	    json:"email"`
	Telefono      string `db:"telefono"    		json:"telefono"`
	Password      string `db:"password"    		json:"password"`
	Celular       string `db:"celular"   		json:"celular"`
	Empresa       string `db:"empresa"    		json:"empresa"`
	Depto         string `db:"depto"   			json:"depto"`
	Suc           string `db:"suc"	  			json:"suc"`
	Numcli        string `db:"numcli"   		json:"numcli"`
  Nivel         string `db:"nivel"   			json:"nivel"`
  Idnivel 	    int   `db:"idnivel"   		  json:"idnivel"`
	Idcliente 	  int   `db:"idcliente"   		json:"idcliente"`
	Nomcli        string `db:"nomcli"   		  json:"nomcli"`
	Idmensajeria  int   `db:"idmensajeria"   json:"idmensajeria"`
	Nommensajeria string `db:"nommensajeria"  json:"nommensajeria"`
  Estatus 		 string `db:"estatus"   		  json:"estatus"`
}

func GetUsuariosWeb(idusuariosweb string) (getuserweb GetUserWeb, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&getuserweb, `SELECT u.idusuariosweb, u.nomuser, u.email as email,
		  u.numcli, u.telefono, u.celular, u.depto, n.nivel, u.password, u.idnivel,
		  COALESCE( u.estatus,'') AS estatus,
		  COALESCE( u.idcliente, 0) AS idcliente,
      COALESCE( u.idmensajeria, 0) AS idmensajeria,
      COALESCE (clientes.nomcli,'') as nomcli, 
      COALESCE (mensajerias.nommensajeria, '') as nommensajeria 
		FROM usuariosweb u 
			left join niveles n on u.idnivel = n.id  
			left join clientes on u.idcliente = clientes.id 
		  left join mensajerias on u.idmensajeria = mensajerias.id
		WHERE idusuariosweb=?`, idusuariosweb)
	return
}

func InsertUsuariosWeb(usuariosweb InsertUsuariosW) (err error) {
	var DB = modeldb.GetDb()

	_, err = DB.NamedExec(`INSERT INTO usuariosweb ( nomuser, email, nivel, telefono, celular, 
			empresa, depto, suc, password, numcli, estatus, idnivel)
		    VALUES( :nomuser, :email, :nivel, :telefono, :celular, 
			:empresa, :depto, :suc, :password, :numcli, :estatus, :idnivel)`, &usuariosweb)
	return
}


func DeleteUsuariosWeb(idusuariosweb string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM usuariosweb
		WHERE idusuariosweb=?`, idusuariosweb)
	return
}

// CAMBIO DE EDGAR
func BuscarUsuario(email string, password string) (userweb []Userweb, err error) {
	var DB = modeldb.GetDb()
	//fmt.Println("email,password ", email, password)
	// err = DB.Select(&usuariosweb, `SELECT u.idusuariosweb, u.nomuser, u.email,
	//           u.numcli, u.telefono, u.celular, u.depto, u.idnivel, n.nivel 
	// 	FROM usuariosweb u left join niveles n on u.idnivel = n.id  
	// 	WHERE email = ? AND password=?`, email, password)

	err = DB.Select(&userweb, `SELECT u.idusuariosweb, u.nomuser, u.email as email,
		  u.numcli, u.telefono, u.celular, u.depto, n.nivel, 
		  COALESCE( u.idcliente, 0) AS idcliente,
		  COALESCE( u.idnivel, 0) AS idnivel,
       COALESCE( u.idmensajeria, 0) AS idmensajeria,
       COALESCE (clientes.nomcli,'') as nomcli, 
       COALESCE (mensajerias.nommensajeria, '') as nommensajeria 
		FROM usuariosweb u 
			left join niveles n on u.idnivel = n.id  
			left join clientes on u.idcliente = clientes.id 
		  left join mensajerias on u.idmensajeria = mensajerias.id
		WHERE u.email = ? AND u.password=? `, email, password)

	return
}




// SELECT u.idusuariosweb, u.nomuser, u.email as email,
// 	   u.numcli, u.telefono, u.celular, u.depto, u.idnivel, n.nivel,
//        u.idcliente, u.idmensajeria, clientes.nomcli, mensajerias.nommensajeria
// FROM usuariosweb u 
// 	left join niveles n on u.idnivel = n.id  
// 	left join clientes on u.idcliente = clientes.id 
//     left join mensajerias on u.idmensajeria = mensajerias.id
// WHERE u.email = "manuel@sait.com.mx" AND u.password="25d55ad283aa400af464c76d713c07ad"


// Para validar login
// func BuscarUsuario(email string, password string) (usuariosweb []Userweb, err error) {
// 	var DB = modeldb.GetDb()
// 	//fmt.Println("email,password ", email, password)
// 	err = DB.Select(&usuariosweb, `SELECT idusuariosweb, nomuser, email, password, numcli, telefono, celular, depto, nivel 
// 		FROM usuariosweb WHERE email = ? AND password=?`, email, password)
// 	return
// }



// func UpdateUsuariosWeb(usuariosweb UsuariosWeb) (err error) {
// 	var DB = modeldb.GetDb()
// 	_, err = DB.NamedExec(`UPDATE usuariosweb
// 		SET nomuser=:nomuser, email=:email, telefono=:telefono, celular=:celular,
// 			empresa=:empresa, depto=:depto, suc=:suc
// 		WHERE idusuariosweb=:idusuariosweb`, usuariosweb)
// 	return
// }
