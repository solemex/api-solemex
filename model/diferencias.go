//Modelo Diferencias

package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
	// "fmt"
	//"github.com/jmoiron/sqlx"
)

type Diferencias struct {
	Id          string `db:"id"     	 	json:"id"`
	Idremesa    int    `db:"idremesa"    	json:"idremesa"`
	Numrem      string `db:"numrem"	 		json:"numrem"`
	Numcli      string `db:"numcli"     	json:"numcli"`
	Nomcli      string `db:"nomcli"         json:"nomcli"`
	Codigo      string `db:"codigo"  	    json:"codigo"`
	Tipotarjeta string `db:"tipotarjeta"    json:"tipotarjeta"`
	Lugarenvio  string `db:"lugarenvio"     json:"lugarenvio"`
	Sucursal    string `db:"sucursal"       json:"sucursal"`
	Iddom       int    `db:"iddom"   	    json:"iddom"`
	Concat2     string `db:"concat2"		json:"concat2"`
	Estatus     int    `db:"estatus"     	json:"estatus"`
	Tipo        string `db:"tipo"     	    json:"tipo"`
}

//TRAER TODO
func ListDiferencias() (diferencias []Diferencias, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&diferencias, `SELECT id, idremesa, COALESCE(numrem,'') as numrem, COALESCE(numcli,'') as numcli,
			 COALESCE(nomcli,'') as nomcli,	COALESCE(codigo,'') as codigo, COALESCE(tipotarjeta,'') as tipotarjeta,
			 COALESCE(lugarenvio,'') as lugarenvio, COALESCE(sucursal,'') as sucursal, COALESCE(iddom,'0') as iddom,
			 COALESCE(concat2,'') as concat2, COALESCE(estatus,'0') as estatus, COALESCE(tipo,'') as tipo FROM diferencias`)
	return
}

func AddDiferencias(diferencias Diferencias) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO diferencias (idremesa, numrem, numcli, nomcli,
				codigo, tipotarjeta, lugarenvio, sucursal, iddom, concat2, estatus,
				tipo)
		VALUES( :idremesa, :numrem, :numcli, :nomcli, :codigo, :tipotarjeta,
			    :lugarenvio, :sucursal, :iddom, :concat2, :estatus, :tipo)`, &diferencias)
	return
}

type Adicionalesdif struct {
	Id          int `db:"id"     	 	json:"id"`
	Numguia     string `db:"numguia"    json:"numguia,omitempty"`
	Idremesa    int    `db:"idremesa"  	json:"idremesa,omitempty"`
	Numrem      string `db:"numrem"	 		json:"numrem,omitempty"`
	Numcli      string `db:"numcli"     json:"numcli,omitempty"`
	Folio    		string `db:"folio"     	json:"folio,omitempty"`
	Nomcli      string  `db:"nomcli"    json:"nomcli,omitempty"`

}


func MarcarDiferencias (ID string) (adicionalesdif []Adicionalesdif, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&adicionalesdif, `select id, 
		COALESCE(numrem,'') as numrem, 
		COALESCE(numcli,'') as numcli,
		COALESCE(nomcli,'') as nomcli,
		COALESCE(numguia,'') as numguia,
		COALESCE(folio, '') as folio, 
		COALESCE(idremesa, 0) as idremesa
		from tarjetas 
		where idremesa = ? and
		tarjetas.numguia IS null; `, ID)
	return
}


type Guiaprincipal struct {
	Id          int `db:"id"     	 	json:"id"`
	Numguia     string `db:"numguia"    json:"numguia,omitempty"`
	Adicional   int     `db:"adicional"    json:"adicional,omitempty"`
}


func FindNumcli (numclip string, idremesap int ) (guiaprincipal Guiaprincipal, err error) {
	// fmt.Println(numclip)
	var DB = modeldb.GetDb()
	err = DB.Get(&guiaprincipal, `select id, 
   	COALESCE(numguia,'') as numguia
		from tarjetas 
		where tarjetas.idremesa = ? and tarjetas.numcli like ? limit 1`, idremesap, numclip)
	return
}

func UpdateAdicional (guiaprincipal Guiaprincipal) (err error) {
	// fmt.Println(guiaprincipal)
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE tarjetas
		SET adicional=1
		WHERE id=:id`, guiaprincipal)
	return
}