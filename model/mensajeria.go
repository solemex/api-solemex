package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
	//"github.com/jmoiron/sqlx"
)

type Idmia struct {
	Id 			  int 	 `db:"id" 			json:"id"`
	Idmensajeria  string `db:"idmensajeria" json:"idmensajeria"`
	Nombre 		  string `db:"nombre"    	json:"nombre"`
}

type Mensajeria struct {
	Id            string `db:"id"     	 			 json:"id"`
	Nummensajeria string `db:"nummensajeria"    	 json:"nummensajeria"`
	Nommensajeria string `db:"nommensajeria"	  	 json:"nommensajeria"`
	Contacto      string `db:"contacto"    			 json:"contacto"`
	Telefono      string `db:"telefono"	  			 json:"telefono"`
	Email         string `db:"email"    			 json:"email"`
	Estado		  string `db:"estado"				 json:"estado"`
	Estatus       int    `db:"estatus"	  			 json:"estatus"`
}


func ListMenxMia(id string) (idmia []Idmia, err error) {
	var DB = modeldb.GetDb()
	fmt.Println(id)
	err = DB.Select(&idmia, `SELECT id, nombre, idmensajeria
		FROM mensajeros
		WHERE estatus = 2 and idmensajeria = ?`, id)
	return
}



func ListMensajeria() (mensajeria []Mensajeria, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&mensajeria, `SELECT id, nummensajeria, nommensajeria, contacto, telefono,
			email, estado, estatus FROM mensajerias`)
	return
}

func GetMensajeria(id string) (mensajeria Mensajeria, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&mensajeria, `SELECT id, nummensajeria, nommensajeria, contacto, telefono,
			email, estado,  estatus FROM mensajerias
		WHERE id=?`, id)
	return
}

func InsertMensajeria(mensajeria Mensajeria) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO mensajerias ( nummensajeria, nommensajeria, contacto, telefono,
			email, estado, estatus  )
		VALUES(:nummensajeria, :nommensajeria, :contacto, :telefono, :email, :estado, :estatus)`, &mensajeria)
	return
}

func UpdateMensajeria(mensajeria Mensajeria) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE mensajerias
		SET nummensajeria=:nummensajeria, nommensajeria=:nommensajeria, contacto=:contacto, telefono=:telefono,
			email=:email, estado=:estado, estatus=:estatus
		WHERE id=:id`, mensajeria)
	return
}

func DeleteMensajeria(id string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM mensajerias
		WHERE id=?`, id)
	return
}
