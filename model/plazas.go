package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type Plaza struct {
	ID            string `db:"id"     	 	 json:"id"`
	Nomplaza      string `db:"nomplaza"	  	 json:"nomplaza"`
	Entidad       string `db:"entidad"    	 json:"entidad"`
	Ciudad        string `db:"ciudad"    	 json:"ciudad"`
	Idmensajeria  int    `db:"idmensajeria"    	 json:"idmensajeria"`
	Nommensajeria string `db:"nommensajeria"    json:"nommensajeria"`
}

func ListPlaza() (plaza []Plaza, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&plaza, `SELECT plazas.id, plazas.entidad, plazas.nomplaza, coalesce(plazas.ciudad,"") AS ciudad, 
	plazas.idmensajeria, coalesce(mensajerias.nommensajeria,"") as nommensajeria
	FROM plazas 
	LEFT JOIN mensajerias on plazas.idmensajeria = mensajerias.id  
    ORDER BY plazas.id`)
	return
}

func GetPlaza(id string) (plaza Plaza, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&plaza, `SELECT plazas.id, plazas.entidad, plazas.nomplaza, coalesce(plazas.ciudad,"") AS ciudad, 
	plazas.idmensajeria, coalesce(mensajerias.nommensajeria,"") as nommensajeria
	FROM plazas 
	LEFT JOIN mensajerias on plazas.id = mensajerias.id  
	WHERE plazas.id=?`, id)
	return
}

func InsertPlaza(plaza Plaza) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO plazas (nomplaza, entidad, ciudad, Idmensajeria )
		VALUES(:nomplaza, :entidad, :ciudad, :Idmensajeria )`, &plaza)
	return
}

func UpdatePlaza(plaza Plaza) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE plazas
		SET entidad=:entidad, nomplaza=:nomplaza, ciudad=:ciudad, idmensajeria=:idmensajeria
		WHERE id=:id`, plaza)
	return
}

func DeletePlaza(id string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM plazas
		WHERE id=?`, id)
	return
}
