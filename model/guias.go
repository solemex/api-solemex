package model

import (
	"gitlab.com/solemex/api-solemex/modeldb"
	_ "github.com/go-sql-driver/mysql"
)

type Guias struct {
	ID          string `db:"id"     	 	 json:"id"`
	Numguia     string `db:"numguia"     	 json:"numguia"`
	Idremesa	int 	`db:"idremesa"     	 json:"idremesa"`
	Numrem      string `db:"numrem"    	     json:"numrem"`
	Numcli      string `db:"numcli"    	 	 json:"numcli"`
	Nomcli      string `db:"nomcli"    	 	 json:"nomcli"`
	Fecha_captura string `db:"fecha_captura" json:"fecha_captura"`
	Latitude	string  `db:"latitude"    	 json:"latitude"`
	Longitude	string `db:"longitude"    	 json:"longitude"`
	Nomrecibe   string `db:"nomrecibe"    	 json:"nomrecibe"`
	Parentesco  string `db:"parentesco"    	 json:"parentesco"`
	Tipoid		string `db:"tipoid"    	 	 json:"tipoid"`
	DireccionEntrega string `db:"direccionEntrega"  json:"direccionEntrega"`
	Numid 		string `db:"numid"    	 	 json:"numid"`
	Fecha_entrega string `db:"fecha_entrega" json:"fecha_entrega"`
	Estatus 	int   `db:"estatus"    	 	 json:"estatus"`
	Nomestatus  string `db:"nomestatus"    	 json:"nomestatus"`
	Hora_entrega string `db:"hora_entrega"   json:"hora_entrega"`
	Concat2 	string `db:"concat2"    	 json:"concat2"`
	Numuser 	string `db:"numuser"    	 json:"numuser"`
	Edit_numuser string `db:"edit_numuser"   json:"edit_numuser"`
	Mensajero   string `db:"mensajero"    	 json:"mensajero"`
	Fecha_asignacion string `db:"fecha_asignacion"  json:"fecha_asignacion"`
	Hora_asignacion string  `db:"hora_asignacion"   json:"hora_asignacion"`
	User_asignacion string `db:"user_asignacion"   	json:"user_asignacion"`
    Num_intentos int   `db:"num_intentos"   json:"num_intentos"`
	Tipoconsig 	string `db:"tipoconsig"    	json:"tipoconsig"`
	Iddom 		string `db:"iddom"    	 	json:"iddom"`
	Codigo 		string `db:"codigo"    	 	json:"codigo"`
	Tipotarjeta string `db:"tipotarjeta"    json:"tipotarjeta"`
	Lugarenvio 	string `db:"lugarenvio"    	json:"lugarenvio"`
	Sucursal 	string `db:"sucursal"    	json:"sucursal"`
	Fecha_print string `db:"fecha_print"    json:"fecha_print"`
	Hora_print  string `db:"hora_print"    	json:"hora_print"`
	Idcliente 	string `db:"idcliente"   	json:"idcliente"`
	Idservicio  string `db:"idservicio"  	json:"idservicio"`
	Idremsuc 	string `db:"idremsuc"   	json:"idremsuc"`
	Idremdom  	string `db:"idremdom"    	json:"idremdom"`
	Idconsuc 	string `db:"idconsuc"    	json:"idconsuc"`
	Idcondom 	string `db:"idcondom"    	json:"idcondom"`
	Paqueteria  string `db:"paqueteria"    	json:"paqueteria"`
	Nomrem		string `db:"nomrem"    	 	json:"nomrem"`
	Remesaam    string `db:"remesaam"    	json:"remesaam"`
	Folio 		string `db:"folio"    		json:"folio"`
	Remfol		string `db:"remfol"    		json:"remfol"`
	Fvenc       string `db:"fvecn"    		json:"fvenc"`
	Cant		int    `db:"cant"    		json:"cant"`
}

type GuiasCon struct {
	ID          string `db:"id"     	 	 json:"id"`
	Numguia     string `db:"numguia"     	 json:"numguia"`
	Idremesa	int 	`db:"idremesa"     	 json:"idremesa"`
	Numrem      string `db:"numrem"    	     json:"numrem"`
	Numcli      string `db:"numcli"    	 	 json:"numcli"`
	Nomcli      string `db:"nomcli"    	 	 json:"nomcli"`
	Fecha_captura string `db:"fecha_captura" json:"fecha_captura"`
	Estatus 	int   `db:"estatus"    	 	 json:"estatus"`
	Nomestatus  string `db:"nomestatus"    	 json:"nomestatus"`
	Concat2 	string `db:"concat2"    	 json:"concat2"`
	Iddom 		string `db:"iddom"    	 	json:"iddom"`
	Codigo 		string `db:"codigo"    	 	json:"codigo"`
	Idcliente 	string `db:"idcliente"   	json:"idcliente"`
	Idservicio  string `db:"idservicio"  	json:"idservicio"`
}


// COALESCE(numremcli, '') as numremcli,

// Consulta general de guias. Mayores a 150
func ListGuias() (guias []GuiasCon, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&guias, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, estatus, concat2,
			iddom, idcliente, idservicio, fecha_captura FROM guias WHERE idremesa > 150`)
	return
}

	// 
func GetGuias(idremesa string) (guias []GuiasCon, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&guias, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, estatus, concat2,
			iddom, idcliente, idservicio, fecha_captura FROM guias WHERE idremesa =?`, idremesa)
	return
}


func GetGuiasRem(idremesa string) (guias []GuiasCon, err error) {
	var DB = modeldb.GetDb()
	
	err = DB.Select(&guias, `SELECT id, numguia, idremesa, numrem, numcli, nomcli, estatus, concat2,
			iddom, idcliente, idservicio, fecha_captura FROM guias WHERE idremesa =?`, idremesa)
	return
}


func InsertGuias (guias Guias) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO guias ( numrem, fingreso, fvenc, fenvio,
			status, na, asig, di, dr, en, tot, numuser, idcliente, idservicio )
		VALUES( :numrem, :fingreso, :fvenc, :fenvio, :status, :na, :asig, :di, :dr, :en, :tot,
                :numuser, :idcliente, :idservicio )`, &guias)
	
	return
}
