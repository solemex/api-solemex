package model

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/solemex/api-solemex/modeldb"
)

type Servicio struct {
	Id          string `db:"id"     	 	 json:"id"`
	Nomserv     string `db:"nomserv"    	 json:"nomserv"`
	Nomcli      string `db:"nomcli"	  		 json:"nomcli"`
	Idcliente   int    `db:"idcliente"	  	 json:"idcliente"`
	Ubicacion   int    `db:"ubicacion"     	 json:"ubicacion"`
	Fecha_hora  int    `db:"fecha_hora"      json:"fecha_hora"`
	Firma       int    `db:"firma"  	     json:"firma"`
	FotoAcuse   int    `db:"fotoacuse"		 json:"fotoacuse"`
	FotoCasa    int    `db:"fotocasa"     	 json:"fotocasa"`
	Usuario_Gen int    `db:"usuario_gen"     json:"usuario_gen"`
	Fecha       string `db:"fecha"   		 json:"fecha"` //DATETIME
	Status      int    `db:"status"			 json:"status"`
}

func ListServicios() (servicio []Servicio, err error) {
	var DB = modeldb.GetDb()
	err = DB.Select(&servicio, `SELECT servicio.id, clientes.nomcli, nomserv, idcliente, ubicacion,
		fecha_hora, firma, fotoacuse, fotocasa, servicio.usuario_gen, servicio.fecha, servicio.status 
		FROM servicio LEFT JOIN clientes on clientes.id = servicio.idcliente`)
	return
}

func GetServicios(id string) (servicio Servicio, err error) {
	var DB = modeldb.GetDb()
	err = DB.Get(&servicio, `SELECT servicio.id, clientes.nomcli, nomserv, idcliente, ubicacion,
		fecha_hora, firma, fotoacuse, fotocasa, servicio.usuario_gen, servicio.fecha, servicio.status 
		FROM servicio LEFT JOIN clientes on clientes.id = servicio.idcliente
		WHERE servicio.id=?`, id)
	return
}

func InsertServicios(servicio Servicio) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`INSERT INTO servicio (nomserv, idcliente, ubicacion,
			fecha_hora, firma, fotoacuse, fotocasa, usuario_gen, fecha, status )
		VALUES( :nomserv, :idcliente, :ubicacion, :fecha_hora, :firma,
			    :fotoacuse, :fotocasa, :usuario_gen, :fecha, :status  )`, &servicio)
	return
}

func UpdateServicios(servicio Servicio) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.NamedExec(`UPDATE servicio
		SET nomserv=:nomserv, idcliente=:idcliente, ubicacion=:ubicacion, fecha_hora=:fecha_hora,
			firma=:firma, fotoacuse=:fotoacuse, fotocasa=:fotocasa, usuario_gen=:usuario_gen, fecha=:fecha,
		    status=:status
		WHERE id=:id`, servicio)
	return
}

func DeleteServicios(id string) (err error) {
	var DB = modeldb.GetDb()
	_, err = DB.Exec(`DELETE FROM servicio
		WHERE id=?`, id)
	return
}
