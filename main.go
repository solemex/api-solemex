package main

import (
	"gitlab.com/solemex/api-solemex/controllers"
	"gitlab.com/solemex/api-solemex/middlewares"
	"gitlab.com/solemex/api-solemex/modeldb"
    "gitlab.com/solemex/api-solemex/funciones"


	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	//"log"
	//"time"
)

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

//var identityKey = "id"

// // User demo
// type User struct {
// 	UserName  string
// 	FirstName string
// 	LastName  string
// 	Email     string
// 	Nomuser   string
// }

// type Userweb struct {
// 	IDusuariosweb string `db:"idusuariosweb"    json:"idusuariosweb"`
// 	Nomuser       string `db:"nomuser"  	    json:"nomuser"`
// 	Email         string `db:"email"	  	    json:"email"`
// 	Password      string `db:"password"    		json:"password"`
// 	Numcli        string `db:"numcli"   		json:"numcli"`
// }

func main() {
	fmt.Println("SOLEMEX API 2019 ")
	modeldb.OpenDB()

	// Middleware
	r := gin.New()
	// r.MaxMultipartMemory = 8 << 40
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middlewares.CORSMiddlewareHandler())

	// REGISTRARSE
	r.POST("/sol/api/v1/usuariosweb", controllers.InsertUsuariosWeb)
	r.POST("/sol/api/v1/buscarusuario", controllers.BuscarUsuario)
	r.POST("/sol/api/v1/getidxmail", controllers.GetidxMail)
    r.GET("/sol/api/v1/activarusuario/:id", controllers.ActivarUsuario)       
	// Ruta utilizada en el modulo de usuarios para filtrar por mensajerias
	r.POST("/sol/api/v1/usuariosxmia", controllers.ListUsuariosxMia)
	r.POST("/sol/api/v1/usuariosxcli", controllers.ListUsuariosxCli)


	//LOGIN
	//r.POST("/api/v1/buscarusuario", controllers.BuscarUsuario)
	// r.POST("/login", controllers.BuscarUsuario)
	r.POST("/sol/login", controllers.BuscarUsuario)

	//CLIENTES
	r.GET("/sol/api/v1/clientes", controllers.ListClientes)
	r.GET("/sol/api/v1/clientes/:id", controllers.GetCliente)
	r.POST("/sol/api/v1/clientes", controllers.InsertCliente)
	r.PUT("/sol/api/v1/clientes/:id", controllers.UpdateCliente)
	r.DELETE("/sol/api/v1/clientes/:id", controllers.DeleteCliente)

	//MENSAJERIA  Convencio Mensajeros = men  Mesjaria = mia
	r.GET("/sol/api/v1/mensajeria", controllers.ListMensajeria)
	r.GET("/sol/api/v1/mensajeria/:id", controllers.GetMensajeria)
	r.POST("/sol/api/v1/mensajeria", controllers.InsertMensajeria)
	r.PUT("/sol/api/v1/mensajeria/:id", controllers.UpdateMensajeria)
	r.DELETE("/sol/api/v1/mensajeria/:id", controllers.DeleteMensajeria)
	r.POST("/sol/api/v1/menxmia", controllers.ListMenxMia)

	//MENSAJEROS
	r.GET("/sol/api/v1/mensajeros", controllers.ListMensajeros)
	r.GET("/sol/api/v1/mensajeros/:id", controllers.GetMensajeros)
	r.POST("/sol/api/v1/mensajeros", controllers.InsertMensajeros)
	r.PUT("/sol/api/v1/mensajeros/:id", controllers.UpdateMensajeros)
	r.DELETE("/sol/api/v1/mensajeros/:id", controllers.DeleteMensajeros)
	// Ruta utilizada en el modulo de mensajeros para filtrar por mensajeria
	r.POST("/sol/api/v1/mensajerosxmia", controllers.ListMensajerosxMia)
	
	// LOGIN APP nuevos controllers
	r.POST("/sol/api/v1/loginapp", controllers.LoginApp)
	r.POST("/sol/api/v1/validausuario", controllers.ValidaInfo)

	// //PLAZA
	r.GET("/sol/api/v1/plazas", controllers.ListPlaza)
	r.GET("/sol/api/v1/plazas/:id", controllers.GetPlaza)
	r.POST("/sol/api/v1/plazas", controllers.InsertPlaza)
	r.PUT("/sol/api/v1/plazas/:id", controllers.UpdatePlaza)
	r.DELETE("/sol/api/v1/plazas/:id", controllers.DeletePlaza)

	//SUCURSALES
	r.GET("/sol/api/v1/catsuc", controllers.ListCatSuc)
	r.POST("/sol/api/v1/sucxnumsuc", controllers.SucxNumsuc)
	r.GET("/sol/api/v1/sucursales", controllers.ListSucursales)
	r.GET("/sol/api/v1/sucursales/:id", controllers.GetSucursales)
	r.POST("/sol/api/v1/sucursales", controllers.InsertSucursales)
	r.PUT("/sol/api/v1/sucursales/:id", controllers.UpdateSucursales)
	r.DELETE("/sol/api/v1/sucursales/:id", controllers.DeleteSucursales)
	// Ruta utilizada en el modulo de sucursales para filtrar por cliente
	r.POST("/sol/api/v1/sucursalesxcli", controllers.ListSucursalesxCli)
    
	

	// //USUARIOS
	// r.GET("/api/v1/usuarios", controllers.ListUsuarios)
	// r.GET("/api/v1/usuarios/:id", controllers.GetUsuarios)
	// r.POST("/api/v1/usuarios", controllers.InsertUsuarios)
	// r.PUT("/api/v1/usuarios/:id", controllers.UpdateUsuarios)

	//NIVELES
	r.GET("/sol/api/v1/niveles", controllers.ListNiveles)
	r.GET("/sol/api/v1/niveles/:id", controllers.GetNiveles)
	r.POST("/sol/api/v1/niveles", controllers.InsertNiveles)
	r.PUT("/sol/api/v1/niveles/:id", controllers.UpdateNiveles)
	r.DELETE("/sol/api/v1/niveles/:id", controllers.DeleteNiveles)

	//SERVICIOS
	r.GET("/sol/api/v1/servicios", controllers.ListServicios)
	r.GET("/sol/api/v1/servicios/:id", controllers.GetServicios)
	r.POST("/sol/api/v1/servicios", controllers.InsertServicios)
	r.PUT("/sol/api/v1/servicios/:id", controllers.UpdateServicios)
	r.DELETE("/sol/api/v1/servicios/:id", controllers.DeleteServicios)

	

	//RASTREOREM  REMESASX PLAZA
	r.GET("/sol/api/v1/remxplaza", controllers.RemxPlaza)
	// r.POST("/sol/api/v1/addremxplaza", controllers.AddRemxPlaza)
	//r.PUT("/sol/api/v1/updateremxplaza", controllers.UpdateRemxPlaza)
	r.POST("/sol/api/v1/cargarremxplaza", controllers.CargarRemxPlaza)	
	//se usa para grabar en base a un id
	r.POST("/sol/api/v1/getcantxestado", controllers.GetCantxEstado)
	r.PUT("/sol/api/v1/updateremxplaza", controllers.UpdateRemxPlaza)

	//AVISOS
	r.GET("/sol/api/v1/avisos", controllers.ListAvisos)
	r.POST("/sol/api/v1/avisos", controllers.InsertAvisos)
	// r.POST("/api/v1/saitavisos", controllers.InsertAviso)
	r.POST("/sol/api/v1/avisosxmens", controllers.AvisosxMens)


	//REMESAS
	r.GET("/sol/api/v1/remesas", controllers.ListRemesas)
	r.GET("/sol/api/v1/remesas/:id", controllers.GetRemesas)
	r.POST("/sol/api/v1/remesas", controllers.InsertRemesas)
	
	//	r.PUT("/api/v1/remesas/:id", controllers.UpdateRemesas)
	r.POST("/sol/api/v1/estatusrem", controllers.EstatusRem) 
	r.POST("/sol/api/v1/estatusremalta", controllers.EstatusRemAlta) 

	r.GET("/sol/api/v1/remxvalidar", controllers.RemxValidar) 
	
	r.PUT("/sol/api/v1/procesarem", controllers.ProcesaRem) 
	r.PUT("/sol/api/v1/cerrarrem", controllers.CerrarRem) 

	//TARJETAS
	r.GET("/sol/api/v1/tarjetas"    , controllers.ListCatTarjetas)
	r.GET("/sol/api/v1/tarjetas/:id", controllers.GetCatTarjetas)
	r.GET("/sol/api/v1/tarjetasxrem/:idremesa", controllers.GetTarxRem)
	r.POST("/sol/api/v1/tarjetaxstatus", controllers.TarjetasxEstatus)
	
	// SUPERVISION 
	r.POST("/sol/api/v1/supervisiontarj", controllers.GetSuperTarj)
	// INTENTOS
	r.POST("/sol/api/v1/supervisionint", controllers.GetSuperInt)

	//Asignar
	r.POST("/sol/api/v1/asignar", controllers.UpdateAsignar)

	//Consulta de Tarjetas por parametros  CONTAR
	r.POST("/sol/api/v1/pendtodas", controllers.ConPendTodas)
	r.POST("/sol/api/v1/pendasig", controllers.ConPendAsig)
	r.POST("/sol/api/v1/pendnoasig", controllers.ConPendNoAsig)
	r.POST("/sol/api/v1/entregadas", controllers.ConEntregadas)
	r.POST("/sol/api/v1/devueltas", controllers.ConDevueltas)

	// Consultas de Tarjetas para la APP Movil ConApp.go
	r.POST("/sol/api/v1/pendxmensajero", controllers.PendxMensajero)
	r.POST("/sol/api/v1/prioridadxmens", controllers.PrioridadxMens)
	r.GET("/sol/api/v1/getsuc/:id", controllers.GetSuc)

	r.PUT("/sol/api/v1/addprioridad", controllers.AddPrioridad)
	

	//Rutas para Post del app Entregar Intentos Devolver  OperApp.go
	r.POST("/sol/api/v1/entregar", controllers.Entregar)
	r.POST("/sol/api/v1/addintento", controllers.AddIntento)
	// r.POST("/api/v1/devolver",   controllers.Devolver)

	//RASTREO
	r.POST("/sol/api/v1/rastreotarjeta", controllers.RastreoTarjeta) //rastreotarjeta
	r.POST("/sol/api/v1/buscarfolio", controllers.BuscarFolio)       // tarjetas.go
	// Ruta utilizada para linkEntregasuc
	r.POST("/sol/api/v1/rastreotarjxsuc", controllers.RastreoTarjxSuc) //rastreotarjeta
	r.POST("/sol/api/v1/rastreointentos", controllers.RastreoIntentos) //rastreotarjeta



	//REPORTES DE SALIDA   (archivosal.go)
	r.POST("/sol/api/v1/archivosal", controllers.ArchivoSal)       

	//VALIDAR REMESA POR PLAZA
	r.POST("/sol/api/v1/busvalidaremxplaza", controllers.BusValidaRemxPlaza)
	r.PUT("/sol/api/v1/correctas", controllers.Correctas)
    r.PUT("/sol/api/v1/devoluciondirecta/:numguia",  controllers.DevolucionDirecta)  
	r.PUT("/sol/api/v1/actualizarem/:id",  controllers.ActualizaRemesa)  
		


	//DIFERENCIAS
	r.GET("/sol/api/v1/diferencias", controllers.ListDiferencias)
	r.POST("/sol/api/v1/addDiferencias", controllers.AddDiferencias)
	r.POST("/sol/api/v1/sobrantes", controllers.Sobrantes)
	r.POST("/sol/api/v1/faltantes", controllers.Faltantes)
	r.PUT("/sol/api/v1/adicionales.update/:id", controllers.MarcarDiferencias)

    //subir imagen  ARCHIVO? ¡?
	r.POST("/sol/api/v1/imagen/firmas", controllers.SubirImagenFirma)       
	r.StaticFS("/sol/imagen/firmas", http.Dir("./imagenes/firmas/"))

	r.POST("/sol/api/v1/imagen/acuses", controllers.SubirImagenAcuses)       
	r.StaticFS("/sol/imagen/acuses", http.Dir("./imagenes/acuses/"))

	r.POST("/sol/api/v1/imagen/domicilios", controllers.SubirImagenDomicilio)    
	r.StaticFS("/sol/imagen/domicilios", http.Dir("./imagenes/domicilios/"))


	r.POST("/sol/api/v1/correos", controllers.EnviarCorreos)       
	r.POST("/sol/api/v1/activarcuenta", controllers.ActivarCuenta)   

	// APP Correos Estilo Amazon
	r.POST("/sol/api/v1/sendentrega", funciones.SendEntrega)       
	r.POST("/sol/api/v1/sendintento", funciones.SendIntento) 
	r.POST("/sol/api/v1/sendentregasuc", funciones.SendEntregaSuc) 
	
	//Plataforma
	r.POST("/sol/api/v1/sendvalidarem", funciones.SendValidaRem) 
	r.POST("/sol/api/v1/sendasignamen", funciones.SendAsignaMen)

	r.GET("/sol/api/v1/getemailxid/:id",   controllers.GetEmailxId)  
	
	r.POST("/sol/api/v1/detalleremesas", controllers.DetalleRemesas)


	// r.PUT("/sol/api/v1/actuazlizatarjeta", controllers.Entregar)
	r.PUT("/sol/api/v1/actuazlizatarjeta", controllers.ActualizaEntrega)

	// VALIDAR SUCURSAL BANREGIO
	// r.POST("/sol/api/v1/getidsucbr",   controllers.GetIdSucBr)
	r.POST("/sol/api/v1/tarjsinsuc"   ,   controllers.TarjSinSuc) 
	r.PUT("/sol/api/v1/putidsucbr/:id",   controllers.PutIdSucBr) 

	auth := r.Group("/sol/auth")
	{

		// MIPERFIL
		auth.GET("/api/v1/usuariosweb", controllers.ListUsuariosWeb)
		auth.GET("/api/v1/usuariosweb/:id", controllers.GetUsuariosWeb)
		auth.PUT("/api/v1/usuariosweb/:id", controllers.UpdateUsuariosWeb)
		auth.DELETE("/api/v1/usuariosweb/:id", controllers.DeleteUsuariosWeb)
	}

	r.Run(":8085") //Nombre del puerto
}
